;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

zero-var leveltime s32

rom-var thinker_functions [10]^actionf_p1 =
	T_Nothing        ; THINK_INVALID
	P_MobjThinker    ; THINK_mobj
	T_MoveCeiling    ; THINK_ceil
	T_MoveFloor      ; THINK_floor
	T_PlatRaise      ; THINK_plat
	T_VerticalDoor   ; THINK_door
	T_FireFlicker    ; THINK_flicker
	T_Glow           ; THINK_glow
	T_LightFlash     ; THINK_flash
	T_StrobeFlash    ; THINK_strobe
end

;
; THINKERS
;
; All thinkers should be allocated by Z_Malloc
; so they can be operated on uniformly.
;
; The actual structures will vary in size,
; but the first element must be thinker_t.
;

; Both the head and tail of the thinker list.
zero-var thinkercap thinker_t


fun T_Nothing (th ^thinker_t)
	; guess what this does!
end


;
; P_InitThinkers
;
fun P_InitThinkers ()
	[thinkercap .prev] = thinkercap
	[thinkercap .next] = thinkercap
end

;
; P_AddThinker
; Adds a new thinker at the end of the list.
;
fun P_AddThinker (th ^thinker_t)
	[th .next] = thinkercap
	[th .prev] = [thinkercap .prev]

	[[th .prev] .next] = th
	[thinkercap .prev] = th

	[th .remove] = 0
end

;
; P_RemoveThinker
;
; Deallocation is lazy -- it will not actually be freed
; until its thinking turn comes up.
;
fun P_RemoveThinker (th ^thinker_t)
	[th .remove] = 1
end

;
; P_RunThinkers
;
fun P_RunThinkers ()
	th = [thinkercap .next]

	loop until eq? th thinkercap
		; time to remove it?
		; andrewj: only if the number of references is zero
		if pos? [th .remove]
			next = [th .next]

			no_refs = TRUE
			if eq? [th .func] THINK_mobj
				mo = raw-cast ^mobj_t th
				no_refs = zero? [mo .ref_count]
			endif

			if no_refs
				[[th .next] .prev] = [th .prev]
				[[th .prev] .next] = [th .next]

				Z_Free th
			endif

			th = next
		else
			idx  = [th .func]
			func = [thinker_functions idx]

			call func th

			th = [th .next]
		endif
	endloop
end

fun P_RemoveAllThinkers ()
	th = [thinkercap .next]

	loop until eq? th thinkercap
		next = [th .next]

		if eq? [th .func] THINK_mobj
			mo = raw-cast ^mobj_t th
			P_RemoveMobj mo
		else
			Z_Free th
		endif

		th = next
	endloop

	P_InitThinkers
end

fun P_IndexForThinker (want_th ^thinker_t -> s32)
	; returns -1 if not found

	idx s32 = 0
	th = [thinkercap .next]

	loop until eq? th thinkercap
		if eq? th want_th
			return idx
		endif

		idx = iadd idx 1
		th  = [th .next]
	endloop

	return -1
end

fun P_LookupThinker (idx s32 -> ^thinker_t)
	if neg? idx
		return NULL
	endif

	th = [thinkercap .next]

	loop until eq? th thinkercap
		if zero? idx
			return th
		endif

		idx = isub idx 1
		th  = [th .next]
	endloop

	return NULL
end
