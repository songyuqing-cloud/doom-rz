;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

; the screen buffer that the v_video.c code draws to.
zero-var dest_screen ^pixel_t

;
; V_DrawPatch
;
; Masks a column based masked pic to the screen.
; andrewj: made it clip to the screen (for F_BunnyScroll).
;
fun V_DrawPatch (x s32 y s32 patch ^patch_t)
	w    = little-endian [patch .width]
	h    = little-endian [patch .height]
	left = little-endian [patch .left-offset]
	top  = little-endian [patch .top-offset]

	x = isub x (conv s32 left)
	y = isub y (conv s32 top)

	col s16 = 0

	loop while lt? col w
		; clip column to the screen
		jump next_col if neg? x
		break if ge? x SCREENWIDTH

		ofs = little-endian [patch .column-ofs col]
		column ^post_t = raw-cast (padd patch ofs)

		; step through the posts in a column
		loop
			delta  = conv s32 [column .top-delta]
			length = conv s32 [column .length]

			break if eq? delta COLUMN_END

			; clip post to the screen (crudely, all or nothing)
			; WISH: clip properly
			y1 = iadd y  delta
			y2 = iadd y1 length

			jump next_post if neg? y1
			jump next_post if gt?  y2 SCREENHEIGHT

			source ^u8 = raw-cast (padd column 3)

			dest = [dest_screen]
			dest = padd dest (imul y1 SCREENWIDTH)
			dest = padd dest x

			count = length
			loop until zero? count
				[dest] = [source]
				dest   = padd dest SCREENWIDTH
				source = padd source 1
				count  = isub count 1
			endloop

		next_post:
			column = padd column length
			column = padd column 4
		endloop

	next_col:
		x   = iadd x 1
		col = iadd col 1
	endloop
end

;
; V_DrawPatchFlipped
; this is only used for F_CastDrawer, so it does no clipping.
;
fun V_DrawPatchFlipped (x s32 y s32 patch ^patch_t)
	w    = little-endian [patch .width]
	h    = little-endian [patch .height]
	left = little-endian [patch .left-offset]
	top  = little-endian [patch .top-offset]

	x = isub x (conv s32 left)
	y = isub y (conv s32 top)

	col s16 = isub w 1

	loop while ge? col 0
		ofs = little-endian [patch .column-ofs col]
		column ^post_t = raw-cast (padd patch ofs)

		; step through the posts in a column
		loop
			delta  = conv s32 [column .top-delta]
			length = conv s32 [column .length]

			break if eq? delta COLUMN_END

			y1 = iadd y delta

			source ^u8 = raw-cast (padd column 3)

			dest = [dest_screen]
			dest = padd dest (imul y1 SCREENWIDTH)
			dest = padd dest x

			count = length
			loop until zero? count
				[dest] = [source]
				dest   = padd dest SCREENWIDTH
				source = padd source 1
				count  = isub count 1
			endloop

			column = padd column length
			column = padd column 4
		endloop

		x   = iadd x 1
		col = isub col 1
	endloop
end

;
; V_DrawBlock
; Draw a linear block of pixels into the view buffer.
; Does no clipping!
;
fun V_DrawBlock (x s32 y s32 width s32 height s32 src ^pixel_t)
	y    = imul y SCREENWIDTH
	dest = padd [dest_screen] y x

	loop while pos? height
		I_MemCopy dest src width

		src    = padd src width
		dest   = padd dest SCREENWIDTH
		height = isub height 1
	endloop
end

;
; V_Init
;
fun V_Init ()
	; no-op!
	; There used to be separate screens that could be drawn to; these are
	; now handled in the upper layers.
end

;
; Set the buffer that the code draws to.
;
fun V_UseBuffer (buffer ^pixel_t)
	[dest_screen] = buffer
end

;
; Restore screen buffer to the i_video screen buffer.
;
fun V_RestoreBuffer ()
	[dest_screen] = [videoBuffer]
end

;
; V_ScreenShot
;
fun V_ScreenShot ()
	palette ^[768]u8 = W_CacheLumpName "PLAYPAL"

	I_WritePCXfile "screenshot.pcx" [videoBuffer] SCREENWIDTH SCREENHEIGHT palette
end
