//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//      Miscellaneous.
//    


#ifndef __M_MISC__
#define __M_MISC__

#include <stdio.h>
#include <stdarg.h>

#include "doomtype.h"

boolean M_WriteFile(char *name, void *source, int length);
char *M_FileCaseExists(char *file);
long M_FileLength(FILE *handle);
void M_ExtractFileBase(char *path, char *dest);

int M_Atoi(const char *str);
void M_ForceUppercase(char *text);
void M_ForceLowercase(char *text);
char *M_StringDuplicate(const char *orig);
boolean M_StringCopy(char *dest, const char *src, unsigned int dest_size);
boolean M_StringConcat(char *dest, const char *src, unsigned int dest_size);
char *M_StringJoin(const char *A, const char *B);
char *M_StringJoin3(const char *A, const char *B, const char *C);
boolean M_StringStartsWith(const char *s, const char *prefix);
boolean M_StringEndsWith(const char *s, const char *suffix);
int M_StrCmp(const char *A, const char *B);
int M_StrCaseCmp(const char *A, const char *B);
int M_StrNCmp(const char *A, const char *B, unsigned int n);
int M_StrNCaseCmp(const char *A, const char *B, unsigned int n);
unsigned int M_StrLen(const char *A);
void M_FormatNum(char *buf, unsigned int buf_len, const char *s, int arg);
char M_ToLower(char ch);
char M_ToUpper(char ch);
unsigned int M_HashName8(char *str, unsigned int total);

#endif

