//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//  Refresh module, data I/O, caching, retrieval of graphics
//  by name.
//


#ifndef __R_DATA__
#define __R_DATA__

#include "r_defs.h"
#include "r_state.h"


// A single patch from a texture definition,
//  basically a rectangular area within
//  the texture rectangle.
typedef struct
{
	short	originx;
	short	originy;
	int		patch;
} texpatch_t;

typedef struct texture_s
{
	// Keep name for switch changing, etc.
	char   name[8];
	short  width;
	short  height;

	int index; // Index in textures list

	int widthmask;

	short          * columnlump;
	unsigned short * columnofs;

	int compositesize;
	byte * composite;   // used when multiple patches are in a single column

	struct texture_s *next; // Next in hash table chain

	// All the patches[patchcount]
	//  are drawn back to front into the cached texture.
	short  patchcount;
	texpatch_t  patches[1];
} texture_t;

extern int numtextures;
extern texture_t **textures;


// Retrieve column data for span blitting.
byte* R_GetColumn ( int  tex, int  col );

fixed_t R_TextureHeight(int tex);


// I/O, setting up the stuff.
void R_InitData (void);
void R_PrecacheLevel (void);


// Retrieval.
// Floor/ceiling opaque texture tiles,
// lookup by name. For animation?
int R_FlatNumForName (char* name);


// Called by P_Ticker for switches and animations,
// returns the texture number for the texture name.
int R_TextureNumForName (char *name);
int R_CheckTextureNumForName (char *name);

#endif
