//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
// Copyright(C)      2021 Andrew Apted
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef MEMIO_H
#define MEMIO_H

typedef struct _MEMFILE MEMFILE;

typedef enum 
{
	MEM_SEEK_SET,
	MEM_SEEK_CUR,
	MEM_SEEK_END,
} mem_rel_t;

MEMFILE * mem_fopen_read(void *buf, unsigned int buflen);
MEMFILE * mem_fopen_write(void);
void mem_freopen_read(MEMFILE *stream);
void mem_fclose(MEMFILE *stream);

unsigned int mem_fread(void *buf, unsigned int size, unsigned int nmemb, MEMFILE *stream);
unsigned int mem_fwrite(const void *ptr, unsigned int size, unsigned int nmemb, MEMFILE *stream);

int mem_ftell(MEMFILE *stream);
int mem_fseek(MEMFILE *stream, int offset, mem_rel_t whence);

#endif /* #ifndef MEMIO_H */
	  
