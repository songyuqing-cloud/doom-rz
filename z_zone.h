//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//      Zone Memory Allocation, perhaps NeXT ObjectiveC inspired.
//


#ifndef __Z_ZONE__
#define __Z_ZONE__

#include <stdio.h>

//
// ZONE MEMORY
// PU - purge tags.

enum
{
	PU_FREE = 1,    // a free block
	PU_CACHE,       // purgeable whenever needed
	PU_STATIC,      // static entire execution time
	PU_INTER,       // static during intermission
	PU_LEVEL,       // static until level exited
};

void    Z_Init (void);
void *  Z_Malloc (int size, int tag);
void *  Z_Calloc (int size, int tag);
void *  Z_MallocCache (int size,  void **user);
void    Z_MoveToCache (void *ptr, void **user);
void    Z_Free (void *ptr);
void    Z_FreeTags (short tag);
void    Z_CheckHeap (void);
int     Z_CalcFreeMemory (void);
int     Z_TotalMemory (void);

#endif
