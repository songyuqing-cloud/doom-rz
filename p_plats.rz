;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const PLATWAIT  = 3
const PLATSPEED = FRACUNIT

type plattype_e s32

const pl_downWaitUpStay = 0
const pl_downWaitBlaze  = 1
const pl_raiseAndChange = 2
const pl_raiseToNearest = 3
const pl_perpetualRaise = 4

type platstatus_e s32

const ps_up        = 0
const ps_down      = 1
const ps_waiting   = 2
const ps_in_stasis = 3

type plat_t struct
	.thinker   thinker_t
	.sector   ^sector_t

	.type      plattype_e
	.status    platstatus_e
	.old_stat  platstatus_e

	.low       fixed_t
	.high      fixed_t
	.speed     fixed_t

	.wait      s32
	.count     s32
	.tag       s16
	.crush     bool
	._pad      [5]u8
end


;
; Move a plat up and down
;
fun T_PlatRaise (P ^plat_t)
	sec = [P .sector]
	t   = iand [leveltime] 7

	if eq? [P .status] ps_up
		res = T_MoveFloorPlane sec [P .speed] [P .high] [P .crush] +1

		raiser = eq? [P .type] pl_raiseAndChange pl_raiseToNearest
		downer = eq? [P .type] pl_downWaitUpStay pl_downWaitBlaze

		if raiser
			if zero? t
				S_StartSound [addr-of sec .soundorg] sfx_stnmov
			endif
		endif

		if and (eq? res RES_crushed) (not [P .crush])
			[P .status] = ps_down
			[P .count]  = [P .wait]

			S_StartSound [addr-of sec .soundorg] sfx_pstart

		elif eq? res RES_pastdest
			[P .status] = ps_waiting
			[P .count]  = [P .wait]

			S_StartSound [addr-of sec .soundorg] sfx_pstop

			if or downer raiser
				P_RemoveActivePlat P
			endif
		endif

		return
	endif

	if eq? [P .status] ps_down
		res = T_MoveFloorPlane sec [P .speed] [P .low] FALSE -1

		if eq? res RES_pastdest
			[P .status] = ps_waiting
			[P .count]  = [P .wait]

			S_StartSound [addr-of sec .soundorg] sfx_pstop
		endif

		return
	endif

	if eq? [P .status] ps_waiting
		[P .count] = isub [P .count] 1

		if zero? [P .count]
			if eq? [sec .floorh] [P .low]
				[P .status] = ps_up
			else
				[P .status] = ps_down
			endif

			S_StartSound [addr-of sec .soundorg] sfx_pstart
		endif

		return
	endif

	; IN STASIS
end

;
; Do Platforms
; the "amount" is only used for raiseAndChange platforms.
;
fun EV_DoPlat (ld ^line_t kind plattype_e amount s32 -> bool)
	did = FALSE

	; activate certain plats that are in_stasis
	if eq? kind pl_perpetualRaise
		P_ActivatePlatInStasis [ld .tag]
	endif

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		if null? [sec .specialdata]
			; new thinker
			P ^plat_t = Z_Calloc plat_t.size PU_LEVEL

			[P .thinker .func] = THINK_plat

			; set parameters, some are adjusted below
			[P .type]   = kind
			[P .sector] = sec
			[P .low]    = 0
			[P .high]   = [sec .floorh]
			[P .speed]  = PLATSPEED
			[P .wait]   = (TICRATE * PLATWAIT)
			[P .count]  = 0
			[P .tag]    = [ld .tag]
			[P .crush]  = FALSE

			sfx sfxenum_t = sfx_pstart

			; Find lowest & highest floors around sector

			if eq? kind pl_raiseToNearest
				[P .status] = ps_up
				[P .speed]  = (PLATSPEED / 2)
				[P .high]   = P_FindNextHighestFloor sec [sec .floorh]
				[P .wait]   = 0

				ref_sec = [[sides] [ld .sidenum 0] .sector]
				[sec .floorpic] = [ref_sec .floorpic]
				[sec .special]  = 0  ; no more damage, if applicable

				sfx = sfx_stnmov

			elif eq? kind pl_raiseAndChange
				[P .status] = ps_up
				[P .speed]  = (PLATSPEED / 2)
				[P .high]   = iadd [sec .floorh] (ishl amount FRACBITS)
				[P .wait]   = 0

				ref_sec = [[sides] [ld .sidenum 0] .sector]
				[sec .floorpic] = [ref_sec .floorpic]

				sfx = sfx_stnmov

			elif eq? kind pl_downWaitUpStay
				[P .status] = ps_down
				[P .speed]  = (PLATSPEED * 4)
				[P .low]    = P_FindLowestFloorSurrounding sec
				[P .low]    = imin [P .low] [sec .floorh]

			elif eq? kind pl_downWaitBlaze
				[P .status] = ps_down
				[P .speed]  = (PLATSPEED * 8)
				[P .low]    = P_FindLowestFloorSurrounding sec
				[P .low]    = imin [P .low] [sec .floorh]

			elif eq? kind pl_perpetualRaise
				[P .low] = P_FindLowestFloorSurrounding sec
				[P .low] = imin [P .low] [sec .floorh]

				[P .high] = P_FindHighestFloorSurrounding sec
				[P .high] = imax [P .high] [sec .floorh]

				; initial direction is random
				r = P_Random
				r = iand r 1
				[P .status] = iadd ps_up r
			endif

			S_StartSound [addr-of sec .soundorg] sfx

			P_AddActivePlat P

			did = TRUE
		endif
	endloop

	return did
end

fun EV_StopPlat (ld ^line_t)
	th = [thinkercap .next]
	loop until eq? th thinkercap
		if eq? [th .func] THINK_plat
			P = raw-cast ^plat_t th

			if and (eq? [P .tag] [ld .tag]) (ne? [P .status] ps_in_stasis)
				[P .old_stat] = [P .status]
				[P .status]   = ps_in_stasis
			endif
		endif
		th = [th .next]
	endloop
end

fun P_ActivatePlatInStasis (tag s16)
	th = [thinkercap .next]
	loop until eq? th thinkercap
		if eq? [th .func] THINK_plat
			P = raw-cast ^plat_t th

			if and (eq? [P .tag] tag) (eq? [P .status] ps_in_stasis)
				[P .status] = [P .old_stat]
			endif
		endif
		th = [th .next]
	endloop
end

fun P_AddActivePlat (P ^plat_t)
	[[P .sector] .specialdata] = P
	P_AddThinker (raw-cast P)
end

fun P_RemoveActivePlat (P ^plat_t)
	[[P .sector] .specialdata] = NULL
	P_RemoveThinker (raw-cast P)
end
