//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 1993-2008 Raven Software
// Copyright(C) 2005-2014 Simon Howard
// Copyright(C)      2021 Andrwe Apted
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#include <stdio.h>
#include <string.h>

#include "i_system.h"
#include "doomtype.h"

extern boolean M_WriteFile(char *name, void *source, int length);

//
// SCREEN SHOTS
//

typedef PACKED_STRUCT(
{
	unsigned char   manufacturer;
	unsigned char   version;
	unsigned char   encoding;
	unsigned char   bits_per_pixel;

	unsigned short  xmin;
	unsigned short  ymin;
	unsigned short  xmax;
	unsigned short  ymax;
	unsigned short  hres;
	unsigned short  vres;

	unsigned char   palette[48];

	unsigned char   reserved;
	unsigned char   color_planes;
	unsigned short  bytes_per_line;
	unsigned short  palette_type;

	unsigned char   _pad[58];
	unsigned char   data[1];   // unbounded
}) pcx_t;


//
// WritePCXfile
//
void I_WritePCXfile(char *filename, byte *data, int width, int height, byte *palette)
{
	int i;

	size_t total_size = width*height*2 + 1000;

	pcx_t *pcx = I_Alloc(total_size);

	I_MemSet(pcx, 0, total_size);

	pcx->manufacturer = 0x0a;  // PCX id
	pcx->version = 5;          // 256 color
	pcx->encoding = 1;         // RLE encoding
	pcx->bits_per_pixel = 8;   // 256 color

	pcx->xmin = 0;
	pcx->ymin = 0;
	pcx->xmax = SHORT(width-1);
	pcx->ymax = SHORT(height-1);
	pcx->hres = SHORT(1);
	pcx->vres = SHORT(1);

	pcx->reserved = 0;        // PCX spec: reserved byte must be zero
	pcx->color_planes = 1;    // chunky image
	pcx->bytes_per_line = SHORT(width);
	pcx->palette_type = SHORT(1);  // not a grey scale

	// pack the image
	byte *pack = &pcx->data[0];

	for (i=0 ; i < width*height ; i++)
	{
		if ( (*data & 0xc0) != 0xc0)
		{
			*pack++ = *data++;
		}
		else
		{
			*pack++ = 0xc1;
			*pack++ = *data++;
		}
	}

	// store the palette
	*pack++ = 0x0c;

	for (i=0 ; i < 768 ; i++)
	{
		*pack++ = *palette++;
	}

	// write output file
	int length = pack - (byte *)pcx;

	M_WriteFile(filename, pcx, length);

	I_Free(pcx);
}
