//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
// Copyright(C)      2021 Andrew Apted
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <io.h>
#ifdef _MSC_VER
#include <direct.h>
#endif
#else
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif

#include "SDL.h"

#include "doomtype.h"
#include "d_main.h"
#include "m_argv.h"
#include "m_config.h"
#include "m_misc.h"

#include "i_sound.h"
#include "i_timer.h"
#include "i_video.h"
#include "i_system.h"


void D_ShutdownEngine ();


signed short SHORT(signed short v)
{
	return v;
}

signed int LONG(signed int v)
{
	return v;
}

signed short BE_SHORT(signed short v)
{
	return SDL_Swap16(v);
}

signed int BE_LONG(signed int v)
{
	return SDL_Swap32(v);
}


//
// Create a directory
//

boolean I_MakeDirectory(const char *path)
{
#ifdef _WIN32
	int res = mkdir(path);
#else
	int res = mkdir(path, 0755);
#endif
	return (res == 0);
}

boolean I_DeleteFile(const char *filename)
{
	int res = remove(filename);
	return (res == 0);
}

boolean I_RenameFile(const char *oldname, const char *newname)
{
	int res = rename(oldname, newname);
	return (res == 0);
}

const char *I_Getenv(const char *name)
{
	return getenv(name);
}

// Tactile feedback function, probably used for the Logitech Cyberman

void I_Tactile(int on, int off, int total)
{
}


//
// I_Print
//

void I_Print (const char *msg)
{
	fputs(msg, stdout);
	fflush(stdout);
}

void I_Print2 (const char *msg, const char *arg)
{
	while (*msg)
	{
		if (msg[0] == '%' && msg[1] == 's')
		{
			fputs(arg, stdout);
			msg += 2;
			continue;
		}

		putchar(*msg);
		msg++;
	}

	fflush(stdout);
}

void I_Print3 (const char *msg, int arg)
{
	while (*msg)
	{
		if (msg[0] == '%' && msg[1] == 'd')
		{
			printf("%d", arg);
			msg += 2;
			continue;
		}

		putchar(*msg);
		msg++;
	}

	fflush(stdout);
}


void I_PrintBanner(char *msg)
{
    int i;
    int spaces = 35 - (M_StrLen(msg) / 2);

    for (i=0; i<spaces; ++i)
        putchar(' ');

    puts(msg);
}

void I_PrintDivider(void)
{
    int i;

    for (i=0; i<75; ++i)
    {
        putchar('=');
    }

    putchar('\n');
}

void I_PrintStartupBanner(char *gamedescription)
{
    I_PrintDivider();
    I_PrintBanner(gamedescription);
    I_PrintDivider();

    I_Print(
    "   Doom-RZ is free software, covered by the GNU General Public License.\n"
    "   There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
    "   PARTICULAR PURPOSE. You are welcome to change and distribute copies\n"
    "   under certain conditions. See the source for more information.\n");

    I_PrintDivider();
}

//
// I_Quit
//

void I_Quit (void)
{
	I_ShutdownGraphics();
	I_ShutdownSound();
	I_ShutdownMusic();

	D_ShutdownEngine();

    SDL_Quit();

    exit(0);
}

//
// I_Error
//

static boolean already_quitting = false;

void I_Error (const char *error)
{
	I_Error2(error, "");
}

void I_Error2 (const char *error, const char *arg)
{
	static char msgbuf[1024];
	char *msg;

	if (already_quitting)
	{
		I_Print("Warning: recursive call to I_Error detected.\n");
		exit(-1);
	}

	already_quitting = true;

    // Write a copy of the message into buffer.
	msg = msgbuf;
	while (*error && msg < &msgbuf[sizeof(msgbuf)-8])
	{
		if (error[0] == '%' && error[1] == 's')
		{
			while (*arg && msg < &msgbuf[sizeof(msgbuf)-8])
			{
				*msg++ = *arg++;
			}
			error += 2;
			continue;
		}

		*msg++ = *error++;
	}
	*msg = 0;

    // Message first.
	fflush(stdout);

	fputs("\n",   stderr);
	fputs(msgbuf, stderr);
	fputs("\n\n", stderr);
    fflush(stderr);

	// Shutdown.  these should never I_Error...

	I_ShutdownGraphics();
	I_ShutdownSound();
	I_ShutdownMusic();

	D_ShutdownEngine();

    // Pop up a GUI dialog box to show the error message, in case
    // the game was not run from the console (and the user would
    // therefore be unable to otherwise see the message).

    if (! M_ParmExists("-nogui"))
    {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Doom-RZ", msgbuf, NULL);
    }

    SDL_Quit();

    exit(-1);
}

//
// Allocation stuff
//

void *I_Alloc(unsigned int size)
{
	void *ptr = malloc(size);

	if (size != 0 && ptr == NULL)
	{
		I_Error ("I_Alloc: out of memory");
	}
	return ptr;
}

void *I_Realloc(void *ptr, unsigned int size)
{
	void *new_ptr = realloc(ptr, size);

	if (size != 0 && new_ptr == NULL)
	{
		I_Error ("I_Realloc: out of memory");
	}
	return new_ptr;
}

void I_Free(void *ptr)
{
	if (ptr != NULL)
	{
		free(ptr);
	}
}

void I_MemSet(void *ptr,  int c, unsigned int size)
{
	memset(ptr, c, size);
}

void I_MemCopy(void *dest, const void *src, unsigned int size)
{
	memcpy(dest, src, size);
}

FILE *I_Fopen(const char *path, const char *mode)
{
	return fopen(path, mode);
}

int I_Fclose(FILE *f)
{
	return fclose(f);
}

boolean I_Feof(FILE *f)
{
	if (feof(f) || ferror(f))
		return 1;
	else
		return 0;
}

void I_Fflush(FILE *f)
{
	fflush(f);
}

int I_Fread(void *buf, int size, FILE *f)
{
	return (int)fread(buf, 1, (size_t)size, f);
}

int I_Fwrite(const void *buf, int size, FILE *f)
{
	return (int)fwrite(buf, 1, (size_t)size, f);
}

int I_Fputc(int c, FILE *f)
{
	return (int)fputc(c, f);
}

int I_Fseek(FILE *f, int offset, int whence)
{
	return fseek(f, (long)offset, whence);
}

int I_Ftell(FILE *f)
{
	return (int)ftell(f);
}

// Check if a file or directory exists.
// returns 'F' for file, 'D' for directory, or 0 if not exist.

char I_FileOrDirExists(const char *filename)
{
	FILE *fstream = fopen(filename, "r");

	if (fstream != NULL)
	{
		fclose(fstream);
		return 'F';
	}

	// If we can't open because the file is a directory, the 
	// "file" exists at least!

	if (errno == EISDIR)
	{
		return 'D';
	}

	return 0;
}
