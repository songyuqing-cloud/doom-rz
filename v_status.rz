;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

extern-var consoleplayer s32

; Size of status bar
const ST_WIDTH  = SCREENWIDTH
const ST_HEIGHT = 32

; Location of status bar
const ST_X = 0
const ST_Y = (SCREENHEIGHT - ST_HEIGHT)

; Number of status faces.
const ST_FACEX = 143
const ST_FACEY = 168

const ST_NUMPAINFACES      = 5
const ST_NUMSTRAIGHTFACES  = 3
const ST_NUMTURNFACES      = 2
const ST_NUMSPECIALFACES   = 3
const ST_NUMEXTRAFACES     = 2

const ST_NUMFACES   = ((ST_FACESTRIDE * ST_NUMPAINFACES) + ST_NUMEXTRAFACES)
const ST_FACESTRIDE = ((ST_NUMSTRAIGHTFACES + ST_NUMTURNFACES) + ST_NUMSPECIALFACES)

const ST_TURNOFFSET     = (ST_NUMSTRAIGHTFACES + 0)
const ST_OUCHOFFSET     = (ST_TURNOFFSET + ST_NUMTURNFACES)
const ST_EVILGRINOFFSET = (ST_OUCHOFFSET + 1)
const ST_RAMPAGEOFFSET  = (ST_EVILGRINOFFSET + 1)
const ST_GODFACE        = (ST_NUMPAINFACES * ST_FACESTRIDE)
const ST_DEADFACE       = (ST_GODFACE + 1)

const ST_EVILGRINCOUNT     = (2 * TICRATE)
const ST_STRAIGHTFACECOUNT = (TICRATE / 2)
const ST_TURNCOUNT         = (1 * TICRATE)
const ST_OUCHCOUNT         = (1 * TICRATE)
const ST_RAMPAGEDELAY      = (2 * TICRATE)

const ST_MUCHPAIN = 20

; probability (out of 256) that the normal face state will change
const ST_FACEPROBABILITY = 96

; AMMO number pos.
const ST_AMMOX         = 44
const ST_AMMOY         = 171

; HEALTH number pos.
const ST_HEALTHX       = 90
const ST_HEALTHY       = 171

; Weapon pos.
const ST_ARMSX0        = 111
const ST_ARMSX1        = (111 + 12)
const ST_ARMSX2        = (111 + 24)
const ST_ARMSY0        = 172
const ST_ARMSY3        = (172 + 10)
const ST_ARMSBGX       = 104
const ST_ARMSBGY       = 168

; Frags pos.
const ST_FRAGSX        = 138
const ST_FRAGSY        = 171

; ARMOR number pos.
const ST_ARMORX        = 221
const ST_ARMORY        = 171

; Key icon positions.
const ST_KEY0X         = 239
const ST_KEY0Y         = 171
const ST_KEY1X         = 239
const ST_KEY1Y         = 181
const ST_KEY2X         = 239
const ST_KEY2Y         = 191

; Ammunition counter.
const ST_AMMO0X        = 288
const ST_AMMO0Y        = 173
const ST_AMMO1X        = 288
const ST_AMMO1Y        = 179
const ST_AMMO2X        = 288
const ST_AMMO2Y        = 191
const ST_AMMO3X        = 288
const ST_AMMO3Y        = 185

; Indicate maximum ammunition.
; Only needed because backpack exists.
const ST_MAXAMMO0X     = 314
const ST_MAXAMMO0Y     = 173
const ST_MAXAMMO1X     = 314
const ST_MAXAMMO1Y     = 179
const ST_MAXAMMO2X     = 314
const ST_MAXAMMO2Y     = 191
const ST_MAXAMMO3X     = 314
const ST_MAXAMMO3Y     = 185

; player for status bar, automap and HUD
zero-var con_pl ^player_t

type StatusFont struct
	.nums   [10]^patch_t
	.percent    ^patch_t
	.minus      ^patch_t
end

zero-var  tall_font StatusFont
zero-var short_font StatusFont
zero-var  gray_font StatusFont

type FaceState struct
	.count    s32  ; tics until face changes
	.priority s32  ; what kind of face to use, a FACE_XXX value
	.pain     s32  ; offset for current health, 0..4
	.turn     s32  ; -1 for left, 0 for middle, +1 for right

	.health   s32  ; detect health changes for appropriately pained face
	.weapons  s32  ; a bitmask of weapons owned by player
	.attack   bool ; detect change to player .attackdown
end

const FACE_DEAD    = 9
const FACE_GRIN    = 8
const FACE_TURNED  = 7
const FACE_SELF    = 6
const FACE_RAMPAGE = 5
const FACE_GOD     = 4
const FACE_NORMAL  = 0

zero-var st_face FaceState

;;
;; STATUS BAR CODE
;;

fun ST_drawBackground ()
	V_DrawPatch ST_X ST_Y (W_CacheLumpName "STBAR")

	if [netgame]
		picname = stack-var [16]uchar
		M_FormatNum picname 16 "STFB%d" [consoleplayer]
		V_DrawPatch ST_FACEX ST_Y (W_CacheLumpName picname)
	endif
end

;
; This is a not-very-pretty routine which handles the face states
; and their timing.
;
; Precedence of expressions is:
;    dead > grin > turned head > self harm > rampage > god mode > normal
;      9      8          7           6          5         4         0
;
fun ST_updateFaceWidget ()
;!	int    i
;!	angle_t  badguyangle
;!	angle_t  diffang
;!	static int  lastattackdown = -1
;!	static int  priority = 0
;!	boolean  doevilgrin
;!
;!	if ([st_face .priority] < 10)
;!	{
;!		; dead
;!		if (!plyr->health)
;!		{
;!			[st_face .priority] = 9
;!			[st_face .index] = ST_DEADFACE
;!			[st_face .count] = 1
;!		}
;!	}
;!
;!	if ([st_face .priority] < 9)
;!	{
;!		if (plyr->bonuscount)
;!		{
;!			; picking up bonus
;!			doevilgrin = false
;!
;!			for (i=0;i<NUMWEAPONS;i++)
;!			{
;!				if (oldweaponsowned[i] != plyr->weaponowned[i])
;!				{
;!					doevilgrin = true
;!					oldweaponsowned[i] = plyr->weaponowned[i]
;!				}
;!			}
;!			if (doevilgrin)
;!			{
;!				; evil grin if just picked up weapon
;!				[st_face .priority] = 8
;!				[st_face .count] = ST_EVILGRINCOUNT
;!				[st_face .index] = ST_calcPainOffset() + ST_EVILGRINOFFSET
;!			}
;!		}
;!
;!	}
;!
;!	if ([st_face .priority] < 8)
;!	{
;!		if (plyr->damagecount
;!				&& plyr->attacker
;!				&& plyr->attacker != plyr->mo)
;!		{
;!			; being attacked
;!			[st_face .priority] = 7
;!
;!			if (plyr->health - st_oldhealth > ST_MUCHPAIN)
;!			{
;!				[st_face .count] = ST_TURNCOUNT
;!				[st_face .index] = ST_calcPainOffset() + ST_OUCHOFFSET
;!			}
;!			else
;!			{
;!				badguyangle = R_PointToAngle2(plyr->mo->x,
;!						plyr->mo->y,
;!						plyr->attacker->x,
;!						plyr->attacker->y)
;!
;!				if (badguyangle > plyr->mo->angle)
;!				{
;!					; whether right or left
;!					diffang = badguyangle - plyr->mo->angle
;!					i = diffang > ANG180
;!				}
;!				else
;!				{
;!					; whether left or right
;!					diffang = plyr->mo->angle - badguyangle
;!					i = diffang <= ANG180
;!				} ; confusing, aint it?
;!
;!
;!				[st_face .count] = ST_TURNCOUNT
;!				[st_face .index] = ST_calcPainOffset()
;!
;!				if (diffang < ANG45)
;!				{
;!					; head-on
;!					[st_face .index] += ST_RAMPAGEOFFSET
;!				}
;!				else if (i)
;!				{
;!					; turn face right
;!					[st_face .index] += ST_TURNOFFSET
;!				}
;!				else
;!				{
;!					; turn face left
;!					[st_face .index] += ST_TURNOFFSET+1
;!				}
;!			}
;!		}
;!	}
;!
;!	if ([st_face .priority] < 7)
;!	{
;!		; getting hurt because of your own damn stupidity
;!		if (plyr->damagecount)
;!		{
;!			if (plyr->health - st_oldhealth > ST_MUCHPAIN)
;!			{
;!				[st_face .priority] = 7
;!				[st_face .count] = ST_TURNCOUNT
;!				[st_face .index] = ST_calcPainOffset() + ST_OUCHOFFSET
;!			}
;!			else
;!			{
;!				[st_face .priority] = 6
;!				[st_face .count] = ST_TURNCOUNT
;!				[st_face .index] = ST_calcPainOffset() + ST_RAMPAGEOFFSET
;!			}
;!
;!		}
;!
;!	}
;!
;!	if ([st_face .priority] < 6)
;!	{
;!		; rapid firing
;!		if (plyr->attackdown)
;!		{
;!			if (lastattackdown==-1)
;!				lastattackdown = ST_RAMPAGEDELAY
;!			else if (!--lastattackdown)
;!			{
;!				[st_face .priority] = 5
;!				[st_face .index] = ST_calcPainOffset() + ST_RAMPAGEOFFSET
;!				[st_face .count] = 1
;!				lastattackdown = 1
;!			}
;!		}
;!		else
;!			lastattackdown = -1
;!
;!	}
;!
;!	if ([st_face .priority] < 5)
;!	{
;!		; invulnerability
;!		if ((plyr->cheats & CF_GODMODE)
;!				|| plyr->powers[pw_invulnerability])
;!		{
;!			[st_face .priority] = 4
;!
;!			[st_face .index] = ST_GODFACE
;!			[st_face .count] = 1
;!
;!		}
;!
;!	}
;!
;!	; look left or look right if the facecount has timed out
;!	if (![st_face .count])
;!	{
;!      r = M_Random
;!		[st_face .index] = ST_calcPainOffset() + (r % 3)
;!		[st_face .count] = ST_STRAIGHTFACECOUNT
;!		[st_face .priority] = 0
;!	}
;!
;!	[st_face .count]--
;!
;!	[st_face .health] = [p .health]
end

;!static int ST_calcPainOffset(void)
;!{
;!	int    health
;!	static int  lastcalc
;!	static int  oldhealth = -1
;!
;!	health = plyr->health > 100 ? 100 : plyr->health
;!
;!	if (health != oldhealth)
;!	{
;!		lastcalc = ST_FACESTRIDE * (((100 - health) * ST_NUMPAINFACES) / 101)
;!		oldhealth = health
;!	}
;!	return lastcalc
;!}

fun ST_drawFace ()
	face = [st_face .priority]
	pain = [st_face .pain]
	turn = [st_face .turn]

	picname = stack-var [16]uchar
	[picname 0] = 0

	if eq? face FACE_DEAD
		M_StringCopy picname "STFDEAD0" 16

	elif eq? face FACE_GRIN
		M_FormatNum picname 16 "STFEVL%d" pain

	elif eq? face FACE_TURNED
		if neg? turn
			M_FormatNum picname 16 "STFTL%d0" pain
		elif pos? turn
			M_FormatNum picname 16 "STFTR%d0" pain
		else
			M_FormatNum picname 16 "STFKILL%d" pain
		endif

	elif eq? face FACE_SELF
		if pos? [st_face .turn]
			M_FormatNum picname 16 "STFOUCH%d" pain
		else
			M_FormatNum picname 16 "STFKILL%d" pain
		endif

	elif eq? face FACE_RAMPAGE
		M_FormatNum picname 16 "STFKILL%d" pain

	elif eq? face FACE_GOD
		M_StringCopy picname "STFGOD0" 16

	else  ; FACE_NORMAL
		M_FormatNum picname 16 "STFST%d0" pain
		[picname 6] = iadd '1' (conv s8 turn)
	endif

	V_DrawPatch ST_FACEX ST_FACEY (W_CacheLumpName picname)
end

fun ST_computeFrags (-> s32)
	; number of frags so far in deathmatch
	frags s32 = 0

	i s32 = 0
	loop while lt? i MAXPLAYERS
		p   = [addr-of players i]
		num = [p .frags i]

		if [p .in_game]
			if eq? i [consoleplayer]
				; account for deaths by environment or own rockets
				frags = isub frags num
			else
				frags = iadd frags num
			endif
		endif

		i = iadd i 1
	endloop

	return frags
end

fun ST_computeWeapons (-> s32)
	mask s32 = 0
	bit  s32 = 0

	i s32 = 0
	loop while lt? i NUMWEAPONS
		if pos? [[con_pl] .owned i]
			bit  = 1
			bit  = ishl bit i
			mask = ior mask bit
		endif
		i = iadd i 1
	endloop

	return mask
end

fun ST_drawWidgets ()
	p    = [con_pl]
	ammo = P_GetPlayerAmmo p

	if ge? ammo 0
		STlib_drawNum ST_AMMOX ST_AMMOY ammo tall_font 3
	endif

	STlib_drawPercent ST_HEALTHX ST_HEALTHY [p .health]
	STlib_drawPercent ST_ARMORX  ST_ARMORY  [p .armorpoints]

	STlib_drawNum ST_AMMO0X ST_AMMO0Y [p .ammo 0] short_font 3
	STlib_drawNum ST_AMMO1X ST_AMMO1Y [p .ammo 1] short_font 3
	STlib_drawNum ST_AMMO2X ST_AMMO2Y [p .ammo 2] short_font 3
	STlib_drawNum ST_AMMO3X ST_AMMO3Y [p .ammo 3] short_font 3

	STlib_drawNum ST_MAXAMMO0X ST_MAXAMMO0Y [p .maxammo 0] short_font 3
	STlib_drawNum ST_MAXAMMO1X ST_MAXAMMO1Y [p .maxammo 1] short_font 3
	STlib_drawNum ST_MAXAMMO2X ST_MAXAMMO2Y [p .maxammo 2] short_font 3
	STlib_drawNum ST_MAXAMMO3X ST_MAXAMMO3Y [p .maxammo 3] short_font 3

	if some? [deathmatch]
		frags = ST_computeFrags

		STlib_drawNum ST_FRAGSX ST_FRAGSY frags tall_font 2
	else
		V_DrawPatch ST_ARMSBGX ST_ARMSBGY (W_CacheLumpName "STARMS")

		ST_drawArm ST_ARMSX0 ST_ARMSY0 2
		ST_drawArm ST_ARMSX1 ST_ARMSY0 3
		ST_drawArm ST_ARMSX2 ST_ARMSY0 4

		ST_drawArm ST_ARMSX0 ST_ARMSY3 5
		ST_drawArm ST_ARMSX1 ST_ARMSY3 6
		ST_drawArm ST_ARMSX2 ST_ARMSY3 7
	endif

	ST_drawKey ST_KEY0X ST_KEY0Y 0
	ST_drawKey ST_KEY1X ST_KEY1Y 1
	ST_drawKey ST_KEY2X ST_KEY2Y 2

	ST_drawFace
end

fun ST_drawKey (x s32 y s32 key s32)
	p = [con_pl]

	; show the skull key if player has both
	skull = iadd key 3
	if [p .cards skull]
		key = skull
	endif

	if [p .cards key]
		picname = stack-var [16]uchar
		M_FormatNum picname 16 "STKEYS%d" key
		V_DrawPatch x y (W_CacheLumpName picname)
	endif
end

fun ST_drawArm (x s32 y s32 digit s32)
	wp = isub digit 1

	if pos? [[con_pl] .owned wp]
		V_DrawPatch x y [short_font .nums digit]
	else
		V_DrawPatch x y [gray_font  .nums digit]
	endif
end

fun STlib_drawPercent (x s32 y s32 value s32)
	V_DrawPatch x y [tall_font .percent]

	STlib_drawNum x y value tall_font 3
end

fun STlib_drawNum (x s32 y s32 num s32 font ^StatusFont width s32)
	is_neg = neg? num

	if is_neg
		num   = ineg num
		width = isub width 1
	endif

	; get width of the '0' digit
	w = conv s32 [[font .nums 0] .width]

	; in the special case of 0, you draw 0
	if zero? num
		x = isub x w
		V_DrawPatch x y [font .nums 0]
		return
	endif

	; draw the new number
	loop while some? num
		break if zero? width
		width = isub width 1

		digit = iremt num 10
		num   = idivt num 10

		x = isub x w
		V_DrawPatch x y [font .nums digit]
	endloop

	; draw a minus sign if necessary
	if ref? [font .minus]
		if is_neg
			x = isub x 8
			V_DrawPatch x y [font .minus]
		endif
	endif
end

fun ST_loadPic (picname ^uchar -> ^patch_t)
	return W_LoadLumpName picname PU_STATIC
end

; Iterates through all graphics to be loaded or unloaded, along with
; the variable they use, invoking the specified callback function.

fun ST_loadGraphics ()
	namebuf = stack-var [16]uchar

	; Load the numbers, tall and short
	i s32 = 0
	loop while lt? i 10
		M_FormatNum namebuf 16  "STTNUM%d" i
		[tall_font .nums i] = ST_loadPic namebuf

		M_FormatNum namebuf 16 "STYSNUM%d" i
		[short_font .nums i] = ST_loadPic namebuf

		M_FormatNum namebuf 16 "STGNUM%d" i
		[gray_font .nums i] = ST_loadPic namebuf

		i = iadd i 1
	endloop

	[tall_font .percent] = ST_loadPic "STTPRCNT"
	[tall_font .minus]   = ST_loadPic "STTMINUS"

	; face states
;!	facenum = 0
;!	for (i=0; i<ST_NUMPAINFACES; i++)
;!	{
;!		for (j=0; j<ST_NUMSTRAIGHTFACES; j++)
;!		{
;!			namebuf[0] = 'S'
;!			namebuf[1] = 'T'
;!			namebuf[2] = 'F'
;!			namebuf[3] = 'S'
;!			namebuf[4] = 'T'
;!			namebuf[5] = '0' + i
;!			namebuf[6] = '0' + j
;!			namebuf[7] = 0
;!			faces[facenum] = ST_loadPic(namebuf)
;!			++facenum
;!		}
;!		M_FormatNum(namebuf, sizeof(namebuf), "STFTR%d0", i);  ; turn right
;!		faces[facenum] = ST_loadPic(namebuf)
;!		++facenum
;!		M_FormatNum(namebuf, sizeof(namebuf), "STFTL%d0", i);  ; turn left
;!		faces[facenum] = ST_loadPic(namebuf)
;!		++facenum
;!		M_FormatNum(namebuf, sizeof(namebuf), "STFOUCH%d", i);  ; ouch!
;!		faces[facenum] = ST_loadPic(namebuf)
;!		++facenum
;!		M_FormatNum(namebuf, sizeof(namebuf), "STFEVL%d", i);  ; evil grin ;)
;!		faces[facenum] = ST_loadPic(namebuf)
;!		++facenum
;!		M_FormatNum(namebuf, sizeof(namebuf), "STFKILL%d", i);  ; pissed off
;!		faces[facenum] = ST_loadPic(namebuf)
;!		++facenum
;!	}
;!
;!	faces[facenum] = ST_loadPic("STFGOD0")
;!	++facenum
;!
;!	faces[facenum] = ST_loadPic("STFDEAD0")
;!	++facenum
end

fun ST_initData ()
	[st_face .priority] = FACE_NORMAL
	[st_face .pain]     = 0
	[st_face .turn]     = 0
	[st_face .health]   = -1
	[st_face .weapons]  = ST_computeWeapons
end

;------------------------------------------------------------------------

#public

fun ST_Ticker ()
	ST_updateFaceWidget
end

fun ST_Drawer (fullscreen bool)
	if or (not fullscreen) [automapactive]
		ST_drawBackground
		ST_drawWidgets
	endif
end

fun ST_Start ()
	[con_pl] = [addr-of players [consoleplayer]]

	ST_initData

	V_PaletteInit
	V_PaletteNormal
end

fun ST_Init ()
	ST_loadGraphics
end
