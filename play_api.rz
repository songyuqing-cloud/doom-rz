;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

extern-fun P_Init ()

extern-var gamemode     GameMode_t
extern-var gamemission  GameMission_t
extern-var gamemission_logical GameMission_t
extern-var gameversion  GameVersion_t
extern-var gamevariant  GameVariant_t

extern-var gameskill    skill_t
extern-var gameepisode  s32
extern-var gamemap      s32

;  p_inter

extern-var maxammo [NUMAMMO]s32

extern-fun P_GivePower (p ^player_t power powertype_t -> bool)

;  p_level

extern-var numvertexes    s32
extern-var vertexes      ^[0]vertex_t

extern-var numsegs        s32
extern-var segs          ^[0]seg_t

extern-var numsectors     s32
extern-var sectors       ^[0]sector_t

extern-var numsubsectors  s32
extern-var subsectors    ^[0]subsector_t

extern-var numnodes       s32
extern-var nodes         ^[0]node_t

extern-var numlines       s32
extern-var lines         ^[0]line_t

extern-var numsides       s32
extern-var sides         ^[0]side_t

extern-fun P_SetupLevel (episode s32 map s32 playermask s32 skill skill_t)

;  p_map

extern-fun P_CheckPosition (mo ^mobj_t x fixed_t y fixed_t -> bool)

;  p_mobj

extern-var nomonsters   bool
extern-var respawnparm  bool
extern-var fastparm     bool

extern-var respawnmonsters  bool

extern-var totalkills   s32
extern-var totalitems   s32
extern-var totalsecret  s32

extern-fun P_SpawnMobj (x fixed_t y fixed_t z fixed_t typenum mobjtype_t -> ^mobj_t)
extern-fun P_AdjustMonsterInfo (fast bool)

extern-fun P_GetState (stnum statenum_t -> ^state_t)
extern-fun P_GetMobjInfo (mtype mobjtype_t -> ^mobjinfo_t)
extern-fun P_ResetBodyQue ()
extern-fun P_AddPlayerCorpse (mo ^mobj_t)

;  p_saveg

extern-var savegamedir ^uchar
extern-var save_stream ^FILE

extern-fun P_SaveGameFile (slot s32 -> ^uchar)
extern-fun P_TempSaveGameFile (-> ^uchar)

extern-fun P_ReadSaveHeader  (-> bool)
extern-fun P_WriteSaveHeader (description ^uchar)

extern-fun P_ArchiveWorld   (-> bool)
extern-fun P_UnArchiveWorld ()

; p_spec

extern-fun P_UpdateSpecials ()

; p_telept

extern-fun P_SpawnVanillaFog (x fixed_t y fixed_t mt_ang s16)

; p_tick

extern-var leveltime s32

extern-fun P_RunThinkers ()

; p_user

extern-var players [MAXPLAYERS]player_t

extern-fun P_PlayerThink (p ^player_t)

; p_weapon

extern-fun P_SetupPsprites (p ^player_t)
extern-fun P_GetPlayerAmmo (p ^player_t -> s32)
