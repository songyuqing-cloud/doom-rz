;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

type lighttable_t u8  ; pixel_t

const MAXWIDTH  = 1120
const MAXHEIGHT = 832

extern-var columnofs  [MAXWIDTH]  s32
extern-var ylookup    [MAXHEIGHT] ^pixel_t
extern-var colormaps  ^[0]pixel_t
extern-var centery    s32
extern-var viewheight s32

; parameters for R_DrawColumn
extern-var dc_source     ^[0]pixel_t
extern-var dc_colormap   ^[0]lighttable_t
extern-var dc_x          s32
extern-var dc_yl         s32
extern-var dc_yh         s32
extern-var dc_iscale     fixed_t
extern-var dc_texturemid fixed_t

; parameters for R_DrawSpan
extern-var ds_y        s32
extern-var ds_x1       s32
extern-var ds_x2       s32
extern-var ds_source   ^[0]pixel_t
extern-var ds_colormap ^[0]lighttable_t
extern-var ds_xfrac    fixed_t
extern-var ds_yfrac    fixed_t
extern-var ds_xstep    fixed_t
extern-var ds_ystep    fixed_t

;;
;; A column is a vertical slice/span from a wall texture that,
;;  given the DOOM style restrictions on the view orientation,
;;  will always have constant z depth.
;; Thus a special case loop for very fast rendering can
;;  be used. It has also been used with Wolfenstein 3D.
;;
fun R_DrawColumn ()
	count = isub [dc_yh] [dc_yl]

	; Zero length, column does not exceed a pixel.
	if neg? count
		return
	endif

	; Framebuffer destination address.
	; Use ylookup LUT to avoid multiply with ScreenWidth.
	; Use columnofs LUT for subwindows?
	dest = [ylookup [dc_yl]]
	dest = padd dest [columnofs [dc_x]]

	; Determine scaling,
	;  which is the only mapping to be done.
	fracstep = [dc_iscale]
	fracofs  = imul (isub [dc_yl] [centery]) fracstep
	frac     = iadd [dc_texturemid] fracofs

	; Inner loop that does the actual texture mapping,
	;  e.g. a DDA-like scaling.
	; This is as fast as it gets.
	loop
		; Re-map color indices from wall texture column
		;  using a lighting/special effects LUT.
		row = ishr frac FRACBITS
		row = iand row 127

		pix    = [[dc_source] row]
		[dest] = [[dc_colormap] pix]

		break if zero? count

		dest  = padd dest SCREENWIDTH
		frac  = iadd frac fracstep
		count = isub count 1
	endloop
end

;;
;; Spectre/Invisibility.
;;
const FUZZTABLE = 50
const +FUZZ = SCREENWIDTH
const -FUZZ = (0 - SCREENWIDTH)

#private

var fuzzpos s32 = 0

rom-var fuzzoffset [FUZZTABLE]s32 =
	+FUZZ -FUZZ +FUZZ -FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ +FUZZ -FUZZ +FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ +FUZZ +FUZZ -FUZZ -FUZZ -FUZZ -FUZZ
	+FUZZ -FUZZ -FUZZ +FUZZ +FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ -FUZZ +FUZZ +FUZZ -FUZZ -FUZZ +FUZZ
	+FUZZ -FUZZ -FUZZ -FUZZ -FUZZ +FUZZ +FUZZ
	+FUZZ +FUZZ -FUZZ +FUZZ +FUZZ -FUZZ +FUZZ
end

#public

;;
;; Framebuffer postprocessing.
;; Creates a fuzzy image by copying pixels
;;  from adjacent ones (vertically).
;; Used with an all black colormap, this
;;  could create the SHADOW effect,
;;  i.e. spectres and invisible players.
;;
fun R_DrawFuzzColumn ()
	dc_yl = [dc_yl]
	dc_yh = [dc_yh]

	; Adjust borders. Low...
	if zero? dc_yl
		dc_yl = 1
	endif

	; ... and high.
	vhm = isub [viewheight] 1
	if ge? dc_yh vhm
		dc_yh = isub [viewheight] 2
	endif

	count = isub dc_yh dc_yl

	; Zero length.
	if neg? count
		return
	endif

	dest = [ylookup dc_yl]
	dest = padd dest [columnofs [dc_x]]

	; Uses colormap #6 (of 0-31) to darken pixels

	colormap = padd [colormaps] 1536

	fuzz = [fuzzpos]

	loop
		; Lookup framebuffer, and retrieve
		;  a pixel that is either one row
		;  above or below of the current one.
		; Add index from colormap to index.
		src    = padd dest [fuzzoffset fuzz]
		pix    = [src]
		[dest] = [colormap pix]

		break if zero? count

		dest  = padd dest SCREENWIDTH
		count = isub count 1

		; Clamp table lookup index.
		fuzz = iadd fuzz 1
		if ge? fuzz FUZZTABLE
			fuzz = 0
		endif
	endloop

	[fuzzpos] = fuzz
end

;;
;; R_DrawSpan
;; With DOOM style restrictions on view orientation,
;;  the floors and ceilings consist of horizontal slices
;;  or spans with constant z depth.
;; However, rotation around the world z axis is possible,
;;  thus this mapping, while simpler and faster than
;;  perspective correct texture mapping, has to traverse
;;  the texture at an angle in all but a few cases.
;; In consequence, flats are not stored by column (like walls),
;;  and the inner loop has to step in texture space u and v.
;;
fun R_DrawSpan ()
	; Pack position and step variables into a single 32-bit integer,
	; with x in the top 16 bits and y in the bottom 16 bits.  For
	; each 16-bit part, the top 6 bits are the integer part and the
	; bottom 10 bits are the fractional part of the pixel position.

	posx = iand (ishl [ds_xfrac] 10) 0xffff0000
	posy = iand (ishr [ds_yfrac]  6) 0x0000ffff
	position u32 = ior posx posy

	stepx = iand (ishl [ds_xstep] 10) 0xffff0000
	stepy = iand (ishr [ds_ystep]  6) 0x0000ffff
	step u32 = ior stepx stepy

	dest = [ylookup [ds_y]]
	dest = padd dest [columnofs [ds_x1]]

	; We do not check for zero spans here?
	count = isub [ds_x2] [ds_x1]

	loop
		; Calculate current texture index in u,v.
		spotx = ishr position 26
		spoty = iand (ishr position 4) 0x0fc0
		spot  = ior spotx spoty

		; Lookup pixel from flat texture tile,
		;  re-index using light/colormap.
		pix    = [[ds_source] spot]
		[dest] = [[ds_colormap] pix]

		break if zero? count

		dest     = padd dest 1
		position = iadd position step
		count    = isub count 1
	endloop
end
