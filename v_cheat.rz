;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public  ; FIXME temporary

extern-var deathmatch s32

extern-fun D_ConsoleMessage (msg ^uchar)  ; FIXME move
extern-fun G_DeferedInitNew (skill skill_t episode s32 map s32)

;;
;; CHEAT SEQUENCE PACKAGE
;;

const MAX_CHEAT_PARAMS = 4

type cheatseq_t struct
	.parameters  u32
	.sequence   ^uchar

	.seq_read  u32
	.par_read  u32
	.params    [MAX_CHEAT_PARAMS]uchar
end

var cheat_mus      cheatseq_t = { 2 "idmus"      ... }
var cheat_god      cheatseq_t = { 0 "iddqd"      ... }
var cheat_ammo1    cheatseq_t = { 0 "idfa"       ... }
var cheat_ammo2    cheatseq_t = { 0 "idkfa"      ... }
var cheat_noclip1  cheatseq_t = { 0 "idspispopd" ... }
var cheat_noclip2  cheatseq_t = { 0 "idclip"     ... }
var cheat_choppers cheatseq_t = { 0 "idchoppers" ... }
var cheat_clev     cheatseq_t = { 2 "idclev"     ... }
var cheat_mypos    cheatseq_t = { 0 "idmypos"    ... }
var cheat_amap     cheatseq_t = { 0 "iddt"       ... }
var cheat_behold   cheatseq_t = { 0 "idbehold"   ... }

var cheat_powerup [6]cheatseq_t =
    { 0 "idbeholdv" ... }
    { 0 "idbeholds" ... }
    { 0 "idbeholdi" ... }
    { 0 "idbeholdr" ... }
    { 0 "idbeholda" ... }
    { 0 "idbeholdl" ... }
end

const STSTR_MUS       = "Music Change"
const STSTR_NOMUS     = "IMPOSSIBLE SELECTION"
const STSTR_GODON     = "Degreelessness Mode On"
const STSTR_GODOFF    = "Degreelessness Mode Off"
const STSTR_KFAADDED  = "Very Happy Ammo Added"
const STSTR_FAADDED   = "Ammo (no keys) Added"
const STSTR_CLIPON    = "No Clipping Mode ON"
const STSTR_CLIPOFF   = "No Clipping Mode OFF"
const STSTR_BEHOLD    = "inVuln, Str, Inviso, Rad, Allmap, or Lite-amp"
const STSTR_BEHOLDX   = "Power-up Toggled"
const STSTR_CHOPPERS  = "... doesn't suck - GM"
const STSTR_CLEV      = "Changing Level..."

zero-var my_position_buf [128]uchar

;
; Called in st_stuff module, which handles the input.
; Returns true if the cheat was successful, false if failed.
;
fun CHT_Check (cheat ^cheatseq_t ascii s32 condition bool -> bool)
	if not condition
		return FALSE
	endif

	ch  = conv u8 ascii
	ch  = M_ToLower ch
	len = M_StrLen [cheat .sequence]

	seq_array ^[0]uchar = [cheat .sequence]

	if lt? [cheat .seq_read] len
		; still reading characters from the cheat code and verifying.
		; reset back to the beginning if a key is wrong.

		if eq? ch [seq_array [cheat .seq_read]]
			[cheat .seq_read] = iadd [cheat .seq_read] 1
		else
			[cheat .seq_read] = 0
		endif

		[cheat .par_read] = 0

	elif lt? [cheat .par_read] [cheat .parameters]
		; we have passed the end of the cheat sequence and are
		; entering parameters now.

		[cheat .params [cheat .par_read]] = ch
		[cheat .par_read] = iadd [cheat .par_read] 1
	endif

	; cheat not matched yet?
	if lt? [cheat .seq_read] len
		return FALSE
	elif lt? [cheat .par_read] [cheat .parameters]
		return FALSE
	endif

	[cheat .seq_read] = 0
	[cheat .par_read] = 0

	return TRUE
end

;
; Respond to keyboard input events, handle cheats.
;
fun CHT_Responder (ev ^event_t -> bool)
	if ne? [ev .type] ev_keydown
		return FALSE
	endif

	not_dm  = zero? [deathmatch]
	not_net = not   [netgame]

	not_nm  = ne? [gameskill] sk_nightmare
	not_nm  = and not_nm not_net

	ascii = [ev .data2]

	; 'iddt' cheat for automap
	if CHT_Check cheat_amap ascii not_dm
		[am_cheat] = iadd  [am_cheat] 1
		[am_cheat] = iremt [am_cheat] 3
	endif

	; 'dqd' cheat for toggleable god mode
	if CHT_Check cheat_god ascii not_nm
		CHT_doGodMode
	endif

	; 'kfa' and 'fa' cheats for full ammo
	if CHT_Check cheat_ammo2 ascii not_nm
		CHT_doFullAmmo TRUE
	elif CHT_Check cheat_ammo1 ascii not_nm
		CHT_doFullAmmo FALSE
	endif

	; 'idspipsopd' and 'idclip' for no clipping
	; andrewj: support both sequences no matter the game
	noclip1 = CHT_Check cheat_noclip1 ascii not_nm
	noclip2 = CHT_Check cheat_noclip2 ascii not_nm

	if or noclip1 noclip2
		CHT_doNoClip
	endif

	; 'behold?' power-up cheats
	pw s32 = 0

	loop while lt? pw 6
		if CHT_Check [addr-of cheat_powerup pw] ascii not_nm
			CHT_doBehold pw
		endif

		pw = iadd pw 1
	endloop

	; 'behold' power-up help string
	if CHT_Check cheat_behold ascii not_nm
		D_ConsoleMessage STSTR_BEHOLD
	endif

	; 'choppers' invulnerability & chainsaw
	if CHT_Check cheat_choppers ascii not_nm
		CHT_doChoppers
	endif

	; 'mypos' for player position
	if CHT_Check cheat_mypos ascii TRUE
		CHT_doMyPos
	endif

	; 'mus' cheat for changing music
	if CHT_Check cheat_mus ascii TRUE
		CHT_doChangeMusic
	endif

	; 'clev' change-level cheat
	if CHT_Check cheat_clev ascii not_net
		CHT_doChangeLevel
	endif

	; cheats never eat the input key
	return FALSE
end

#private

fun CHT_doFullAmmo (kfa bool)
	p = [con_pl]

	i s32 = 0
	loop while lt? i NUMWEAPONS
		[p .owned i] = 1
		i = iadd i 1
	endloop

	i s32 = 0
	loop while lt? i NUMAMMO
		[p .ammo i] = [p .maxammo i]
		i = iadd i 1
	endloop

	if kfa
		i s32 = 0
		loop while lt? i NUMCARDS
			[p .cards i] = TRUE
			i = iadd i 1
		endloop
	endif

	if kfa
		[p .armorpoints] = deh_idkfa_armor
		[p .armortype]   = deh_idkfa_armor_class

		D_ConsoleMessage STSTR_KFAADDED
	else
		[p .armorpoints] = deh_idfa_armor
		[p .armortype]   = deh_idfa_armor_class

		D_ConsoleMessage STSTR_FAADDED
	endif
end

fun CHT_doGodMode ()
	p  = [con_pl]
	mo = [p .mo]

	[p .cheats] = ixor [p .cheats] CF_GODMODE

	if some? (iand [p .cheats] CF_GODMODE)
		[p .health] = deh_god_mode_health

		if ref? mo
			[mo .health] = [p .health]
		endif

		D_ConsoleMessage STSTR_GODON
	else
		D_ConsoleMessage STSTR_GODOFF
	endif
end

fun CHT_doNoClip ()
	p = [con_pl]
	[p .cheats] = ixor [p .cheats] CF_NOCLIP

	if some? (iand [p .cheats] CF_NOCLIP)
		D_ConsoleMessage STSTR_CLIPON
	else
		D_ConsoleMessage STSTR_CLIPOFF
	endif
end

fun CHT_doBehold (pw s32)
	p = [con_pl]

	if zero? [p .powers pw]
		P_GivePower p pw
	elif eq? pw pw_strength
		[p .powers pw] = 0
	else
		; let it stop on next game tic, turning off effects
		[p .powers pw] = 1
	endif

	D_ConsoleMessage STSTR_BEHOLDX
end

fun CHT_doChoppers ()
	p = [con_pl]
	[p .owned  wp_chainsaw] = 1

	; remove the Invuln sphere (at next game tic)
	[p .powers pw_invulnerability] = 1

	D_ConsoleMessage STSTR_CHOPPERS
end

fun CHT_doMyPos ()
	mo = [[con_pl] .mo]

	x  = idivt [mo .x] FRACUNIT
	y  = idivt [mo .y] FRACUNIT
	z  = idivt [mo .z] FRACUNIT

	ang = conv s32 [mo .angle]
	ang = idivt ang 11930465

	xstr = stack-var [32]uchar
	ystr = stack-var [32]uchar
	zstr = stack-var [32]uchar
	astr = stack-var [32]uchar

	M_FormatNum xstr 32 "x %d, " x
	M_FormatNum ystr 32 "y %d, " y
	M_FormatNum zstr 32 "z %d, " y
	M_FormatNum astr 32 "ang %d" ang

	[my_position_buf 0] = 0

	CHT_concatPosStr xstr
	CHT_concatPosStr ystr
	CHT_concatPosStr zstr
	CHT_concatPosStr astr

	D_ConsoleMessage my_position_buf
end

fun CHT_concatPosStr (s ^uchar)
	len = M_StrLen my_position_buf
	d   = [addr-of my_position_buf len]

	loop
		[d] = [s]
		break if zero? [d]

		s = padd s 1
		d = padd d 1
	endloop
end

fun CHT_doChangeMusic ()
	D_ConsoleMessage STSTR_NOMUS

	high = conv s32 [cheat_mus .params 0]
	low  = conv s32 [cheat_mus .params 1]

	high = isub high '0'
	low  = isub low  '0'

	if or (neg? high) (gt? high 9)
		return
	elif or (neg? low) (gt? low 9)
		return
	endif

	; Note: The original v1.9 had a bug that tried to play back
	; the Doom II music regardless of gamemode.  This was fixed
	; in the Ultimate Doom executable so that it would work for
	; the Doom 1 music as well.

	if eq? [gamemode] commercial
		high   = imul high 10
		musnum = iadd mus_runnin high low -1

		if ge? musnum NUMMUSIC
			return
		endif
	else
		high   = isub high 1
		high   = imul high 9
		musnum = iadd mus_e1m1 high low -1

		if gt? musnum mus_victor
			return
		endif
	endif

	if le? musnum 0
		return
	endif

	S_ChangeMusic musnum TRUE
	D_ConsoleMessage STSTR_MUS
end

fun CHT_doChangeLevel ()
	epi = conv s32 [cheat_clev .params 0]
	map = conv s32 [cheat_clev .params 1]

	epi = isub epi '0'
	map = isub map  '0'

	if or (neg? epi) (gt? epi 9)
		return
	elif or (neg? map) (gt? map 9)
		return
	endif

	if eq? [gamemode] commercial
		; Doom II

		epi = imul epi 10
		map = iadd epi map
		epi = 0

		; catch invalid maps.
		if lt? map 1
			return
		elif gt? map 40
			return
		endif
	else
		; Doom I

		; Chex.exe always warps to episode 1.
		if eq? [gameversion] exe_chex
			epi = imin epi 1
			map = imin map 5
		endif

		; catch invalid maps.
		if lt? epi 1
			return
		elif gt? epi 4
			return
		elif eq? epi 4
			if lt? [gameversion] exe_ultimate
				return
			endif
		endif

		if lt? map 1
			return
		elif gt? map 9
			return
		endif
	endif

	; so be it.
	D_ConsoleMessage STSTR_CLEV
	G_DeferedInitNew [gameskill] epi map
end
