//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:  Heads-up displays
//


#include "doomdef.h"
#include "doomkeys.h"

#include "z_zone.h"

#include "i_input.h"
#include "i_system.h"
#include "i_video.h"

#include "hu_stuff.h"
#include "hu_lib.h"
#include "m_controls.h"
#include "m_misc.h"
#include "w_wad.h"

#include "s_sound.h"

#include "doomstat.h"

// Data.
#include "sounds.h"

//
// Locally used constants, shortcuts.
//
#define HU_TITLE	(mapnames[(gameepisode-1)*9+gamemap-1])
#define HU_TITLE2	(mapnames_commercial[gamemap-1])
#define HU_TITLEP	(mapnames_commercial[gamemap-1 + 32])
#define HU_TITLET	(mapnames_commercial[gamemap-1 + 64])
#define HU_TITLE_CHEX   (mapnames_chex[(gameepisode-1)*9+gamemap-1])
#define HU_TITLEHEIGHT	1
#define HU_TITLEX	0
#define HU_TITLEY	(167 - SHORT(hu_font[0]->height))

#define HU_INPUTTOGGLE	't'
#define HU_INPUTX	HU_MSGX
#define HU_INPUTY	(HU_MSGY + HU_MSGHEIGHT*(SHORT(hu_font[0]->height) +1))
#define HU_INPUTWIDTH	64
#define HU_INPUTHEIGHT	1

//
// strings...
//
#define HUSTR_MSGU    "[Message unsent]"

#define HUSTR_E1M1    "E1M1: Hangar"
#define HUSTR_E1M2    "E1M2: Nuclear Plant"
#define HUSTR_E1M3    "E1M3: Toxin Refinery"
#define HUSTR_E1M4    "E1M4: Command Control"
#define HUSTR_E1M5    "E1M5: Phobos Lab"
#define HUSTR_E1M6    "E1M6: Central Processing"
#define HUSTR_E1M7    "E1M7: Computer Station"
#define HUSTR_E1M8    "E1M8: Phobos Anomaly"
#define HUSTR_E1M9    "E1M9: Military Base"

#define HUSTR_E2M1    "E2M1: Deimos Anomaly"
#define HUSTR_E2M2    "E2M2: Containment Area"
#define HUSTR_E2M3    "E2M3: Refinery"
#define HUSTR_E2M4    "E2M4: Deimos Lab"
#define HUSTR_E2M5    "E2M5: Command Center"
#define HUSTR_E2M6    "E2M6: Halls of the Damned"
#define HUSTR_E2M7    "E2M7: Spawning Vats"
#define HUSTR_E2M8    "E2M8: Tower of Babel"
#define HUSTR_E2M9    "E2M9: Fortress of Mystery"

#define HUSTR_E3M1    "E3M1: Hell Keep"
#define HUSTR_E3M2    "E3M2: Slough of Despair"
#define HUSTR_E3M3    "E3M3: Pandemonium"
#define HUSTR_E3M4    "E3M4: House of Pain"
#define HUSTR_E3M5    "E3M5: Unholy Cathedral"
#define HUSTR_E3M6    "E3M6: Mt. Erebus"
#define HUSTR_E3M7    "E3M7: Limbo"
#define HUSTR_E3M8    "E3M8: Dis"
#define HUSTR_E3M9    "E3M9: Warrens"

#define HUSTR_E4M1    "E4M1: Hell Beneath"
#define HUSTR_E4M2    "E4M2: Perfect Hatred"
#define HUSTR_E4M3    "E4M3: Sever The Wicked"
#define HUSTR_E4M4    "E4M4: Unruly Evil"
#define HUSTR_E4M5    "E4M5: They Will Repent"
#define HUSTR_E4M6    "E4M6: Against Thee Wickedly"
#define HUSTR_E4M7    "E4M7: And Hell Followed"
#define HUSTR_E4M8    "E4M8: Unto The Cruel"
#define HUSTR_E4M9    "E4M9: Fear"

#define HUSTR_1    "level 1: entryway"
#define HUSTR_2    "level 2: underhalls"
#define HUSTR_3    "level 3: the gantlet"
#define HUSTR_4    "level 4: the focus"
#define HUSTR_5    "level 5: the waste tunnels"
#define HUSTR_6    "level 6: the crusher"
#define HUSTR_7    "level 7: dead simple"
#define HUSTR_8    "level 8: tricks and traps"
#define HUSTR_9    "level 9: the pit"
#define HUSTR_10    "level 10: refueling base"
#define HUSTR_11    "level 11: 'o' of destruction!"

#define HUSTR_12    "level 12: the factory"
#define HUSTR_13    "level 13: downtown"
#define HUSTR_14    "level 14: the inmost dens"
#define HUSTR_15    "level 15: industrial zone"
#define HUSTR_16    "level 16: suburbs"
#define HUSTR_17    "level 17: tenements"
#define HUSTR_18    "level 18: the courtyard"
#define HUSTR_19    "level 19: the citadel"
#define HUSTR_20    "level 20: gotcha!"

#define HUSTR_21    "level 21: nirvana"
#define HUSTR_22    "level 22: the catacombs"
#define HUSTR_23    "level 23: barrels o' fun"
#define HUSTR_24    "level 24: the chasm"
#define HUSTR_25    "level 25: bloodfalls"
#define HUSTR_26    "level 26: the abandoned mines"
#define HUSTR_27    "level 27: monster condo"
#define HUSTR_28    "level 28: the spirit world"
#define HUSTR_29    "level 29: the living end"
#define HUSTR_30    "level 30: icon of sin"

#define HUSTR_31    "level 31: wolfenstein"
#define HUSTR_32    "level 32: grosse"

#define PHUSTR_1    "level 1: congo"
#define PHUSTR_2    "level 2: well of souls"
#define PHUSTR_3    "level 3: aztec"
#define PHUSTR_4    "level 4: caged"
#define PHUSTR_5    "level 5: ghost town"
#define PHUSTR_6    "level 6: baron's lair"
#define PHUSTR_7    "level 7: caughtyard"
#define PHUSTR_8    "level 8: realm"
#define PHUSTR_9    "level 9: abattoire"
#define PHUSTR_10    "level 10: onslaught"
#define PHUSTR_11    "level 11: hunted"

#define PHUSTR_12    "level 12: speed"
#define PHUSTR_13    "level 13: the crypt"
#define PHUSTR_14    "level 14: genesis"
#define PHUSTR_15    "level 15: the twilight"
#define PHUSTR_16    "level 16: the omen"
#define PHUSTR_17    "level 17: compound"
#define PHUSTR_18    "level 18: neurosphere"
#define PHUSTR_19    "level 19: nme"
#define PHUSTR_20    "level 20: the death domain"

#define PHUSTR_21    "level 21: slayer"
#define PHUSTR_22    "level 22: impossible mission"
#define PHUSTR_23    "level 23: tombstone"
#define PHUSTR_24    "level 24: the final frontier"
#define PHUSTR_25    "level 25: the temple of darkness"
#define PHUSTR_26    "level 26: bunker"
#define PHUSTR_27    "level 27: anti-christ"
#define PHUSTR_28    "level 28: the sewers"
#define PHUSTR_29    "level 29: odyssey of noises"
#define PHUSTR_30    "level 30: the gateway of hell"

#define PHUSTR_31    "level 31: cyberden"
#define PHUSTR_32    "level 32: go 2 it"

#define THUSTR_1    "level 1: system control"
#define THUSTR_2    "level 2: human bbq"
#define THUSTR_3    "level 3: power control"
#define THUSTR_4    "level 4: wormhole"
#define THUSTR_5    "level 5: hanger"
#define THUSTR_6    "level 6: open season"
#define THUSTR_7    "level 7: prison"
#define THUSTR_8    "level 8: metal"
#define THUSTR_9    "level 9: stronghold"
#define THUSTR_10    "level 10: redemption"
#define THUSTR_11    "level 11: storage facility"

#define THUSTR_12    "level 12: crater"
#define THUSTR_13    "level 13: nukage processing"
#define THUSTR_14    "level 14: steel works"
#define THUSTR_15    "level 15: dead zone"
#define THUSTR_16    "level 16: deepest reaches"
#define THUSTR_17    "level 17: processing area"
#define THUSTR_18    "level 18: mill"
#define THUSTR_19    "level 19: shipping/respawning"
#define THUSTR_20    "level 20: central processing"

#define THUSTR_21    "level 21: administration center"
#define THUSTR_22    "level 22: habitat"
#define THUSTR_23    "level 23: lunar mining project"
#define THUSTR_24    "level 24: quarry"
#define THUSTR_25    "level 25: baron's den"
#define THUSTR_26    "level 26: ballistyx"
#define THUSTR_27    "level 27: mount pain"
#define THUSTR_28    "level 28: heck"
#define THUSTR_29    "level 29: river styx"
#define THUSTR_30    "level 30: last call"

#define THUSTR_31    "level 31: pharaoh"
#define THUSTR_32    "level 32: caribbean"

#define HUSTR_CHATMACRO1    "I'm ready to kick butt!"
#define HUSTR_CHATMACRO2    "I'm OK."
#define HUSTR_CHATMACRO3    "I'm not looking too good!"
#define HUSTR_CHATMACRO4    "Help!"
#define HUSTR_CHATMACRO5    "You suck!"
#define HUSTR_CHATMACRO6    "Next time, scumbag..."
#define HUSTR_CHATMACRO7    "Come here!"
#define HUSTR_CHATMACRO8    "I'll take care of it."
#define HUSTR_CHATMACRO9    "Yes"
#define HUSTR_CHATMACRO0    "No"

#define HUSTR_TALKTOSELF1    "You mumble to yourself"
#define HUSTR_TALKTOSELF2    "Who's there?"
#define HUSTR_TALKTOSELF3    "You scare yourself"
#define HUSTR_TALKTOSELF4    "You start to rave"
#define HUSTR_TALKTOSELF5    "You've lost it..."

#define HUSTR_MESSAGESENT    "[Message Sent]"

// The following should NOT be changed unless it seems
// just AWFULLY necessary

#define HUSTR_PLRGREEN   "Green: "
#define HUSTR_PLRINDIGO  "Indigo: "
#define HUSTR_PLRBROWN   "Brown: "
#define HUSTR_PLRRED     "Red: "

#define HUSTR_KEYGREEN   'g'
#define HUSTR_KEYINDIGO  'i'
#define HUSTR_KEYBROWN   'b'
#define HUSTR_KEYRED     'r'


char *chat_macros[10] =
{
    HUSTR_CHATMACRO0,
    HUSTR_CHATMACRO1,
    HUSTR_CHATMACRO2,
    HUSTR_CHATMACRO3,
    HUSTR_CHATMACRO4,
    HUSTR_CHATMACRO5,
    HUSTR_CHATMACRO6,
    HUSTR_CHATMACRO7,
    HUSTR_CHATMACRO8,
    HUSTR_CHATMACRO9
};

char*	player_names[] =
{
    HUSTR_PLRGREEN,
    HUSTR_PLRINDIGO,
    HUSTR_PLRBROWN,
    HUSTR_PLRRED
};

char			chat_char; // remove later.
static player_t*	plr;
patch_t*		hu_font[HU_FONTSIZE];
static hu_textline_t	w_title;
boolean			chat_on;
static hu_itext_t	w_chat;
static boolean		always_off = false;
static char		chat_dest[MAXPLAYERS];
static hu_itext_t w_inputbuffer[MAXPLAYERS];

static boolean		message_on;
boolean			message_force;  // user toggled the MESSAGES option, force showing its message
static boolean		message_no_overwrite;  // current message has priority over new ones

static hu_stext_t	w_message;
static int		message_counter;

extern int		showMessages;

static boolean		headsupactive = false;

//
// Builtin map names.
// The actual names can be found in DStrings.h.
//

char*	mapnames[] =	// DOOM shareware/registered/retail (Ultimate) names.
{

    HUSTR_E1M1,
    HUSTR_E1M2,
    HUSTR_E1M3,
    HUSTR_E1M4,
    HUSTR_E1M5,
    HUSTR_E1M6,
    HUSTR_E1M7,
    HUSTR_E1M8,
    HUSTR_E1M9,

    HUSTR_E2M1,
    HUSTR_E2M2,
    HUSTR_E2M3,
    HUSTR_E2M4,
    HUSTR_E2M5,
    HUSTR_E2M6,
    HUSTR_E2M7,
    HUSTR_E2M8,
    HUSTR_E2M9,

    HUSTR_E3M1,
    HUSTR_E3M2,
    HUSTR_E3M3,
    HUSTR_E3M4,
    HUSTR_E3M5,
    HUSTR_E3M6,
    HUSTR_E3M7,
    HUSTR_E3M8,
    HUSTR_E3M9,

    HUSTR_E4M1,
    HUSTR_E4M2,
    HUSTR_E4M3,
    HUSTR_E4M4,
    HUSTR_E4M5,
    HUSTR_E4M6,
    HUSTR_E4M7,
    HUSTR_E4M8,
    HUSTR_E4M9,

    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL"
};

char*   mapnames_chex[] =   // Chex Quest names.
{

    HUSTR_E1M1,
    HUSTR_E1M2,
    HUSTR_E1M3,
    HUSTR_E1M4,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,

    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,

    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,

    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,
    HUSTR_E1M5,

    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL",
    "NEWLEVEL"
};

// List of names for levels in commercial IWADs
// (doom2.wad, plutonia.wad, tnt.wad).  These are stored in a
// single large array; WADs like pl2.wad have a MAP33, and rely on
// the layout in the Vanilla executable, where it is possible to
// overflow the end of one array into the next.

char *mapnames_commercial[] =
{
    // DOOM 2 map names.

    HUSTR_1,
    HUSTR_2,
    HUSTR_3,
    HUSTR_4,
    HUSTR_5,
    HUSTR_6,
    HUSTR_7,
    HUSTR_8,
    HUSTR_9,
    HUSTR_10,
    HUSTR_11,
	
    HUSTR_12,
    HUSTR_13,
    HUSTR_14,
    HUSTR_15,
    HUSTR_16,
    HUSTR_17,
    HUSTR_18,
    HUSTR_19,
    HUSTR_20,
	
    HUSTR_21,
    HUSTR_22,
    HUSTR_23,
    HUSTR_24,
    HUSTR_25,
    HUSTR_26,
    HUSTR_27,
    HUSTR_28,
    HUSTR_29,
    HUSTR_30,
    HUSTR_31,
    HUSTR_32,

    // Plutonia WAD map names.

    PHUSTR_1,
    PHUSTR_2,
    PHUSTR_3,
    PHUSTR_4,
    PHUSTR_5,
    PHUSTR_6,
    PHUSTR_7,
    PHUSTR_8,
    PHUSTR_9,
    PHUSTR_10,
    PHUSTR_11,
	
    PHUSTR_12,
    PHUSTR_13,
    PHUSTR_14,
    PHUSTR_15,
    PHUSTR_16,
    PHUSTR_17,
    PHUSTR_18,
    PHUSTR_19,
    PHUSTR_20,
	
    PHUSTR_21,
    PHUSTR_22,
    PHUSTR_23,
    PHUSTR_24,
    PHUSTR_25,
    PHUSTR_26,
    PHUSTR_27,
    PHUSTR_28,
    PHUSTR_29,
    PHUSTR_30,
    PHUSTR_31,
    PHUSTR_32,
    
    // TNT WAD map names.

    THUSTR_1,
    THUSTR_2,
    THUSTR_3,
    THUSTR_4,
    THUSTR_5,
    THUSTR_6,
    THUSTR_7,
    THUSTR_8,
    THUSTR_9,
    THUSTR_10,
    THUSTR_11,
	
    THUSTR_12,
    THUSTR_13,
    THUSTR_14,
    THUSTR_15,
    THUSTR_16,
    THUSTR_17,
    THUSTR_18,
    THUSTR_19,
    THUSTR_20,
	
    THUSTR_21,
    THUSTR_22,
    THUSTR_23,
    THUSTR_24,
    THUSTR_25,
    THUSTR_26,
    THUSTR_27,
    THUSTR_28,
    THUSTR_29,
    THUSTR_30,
    THUSTR_31,
    THUSTR_32
};

void HU_Init(void)
{
	int		i;
	int		ch;
	char	buffer[10];

	// load the heads-up font
	ch = HU_FONTSTART;
	for (i=0; i<HU_FONTSIZE; i++, ch++)
	{
		if (ch < 100)
			M_FormatNum(buffer, sizeof(buffer), "STCFN0%d", ch);
		else
			M_FormatNum(buffer, sizeof(buffer), "STCFN%d", ch);

		hu_font[i] = (patch_t *) W_LoadLumpName(buffer, PU_STATIC);
	}
}

void HU_Stop(void)
{
	headsupactive = false;
}

void HU_Start(void)
{

	int		i;
	char*	s;

	if (headsupactive)
		HU_Stop();

	plr = &players[consoleplayer];
	message_on = false;
	message_force = false;
	message_no_overwrite = false;
	chat_on = false;

	// create the message widget
	HUlib_initSText(&w_message,
			HU_MSGX, HU_MSGY, HU_MSGHEIGHT,
			hu_font,
			HU_FONTSTART, &message_on);

	// create the map title widget
	HUlib_initTextLine(&w_title,
			HU_TITLEX, HU_TITLEY,
			hu_font,
			HU_FONTSTART);

	switch ( gamemission_logical )
	{
		case mi_doom: s = HU_TITLE; break;
		case mi_doom2: s = HU_TITLE2; break;
		case mi_pack_plut: s = HU_TITLEP; break;
		case mi_pack_tnt: s = HU_TITLET; break;
		default: s = "Unknown level"; break;
	}

	if (gamemission_logical == mi_doom && gameversion == exe_chex)
	{
		s = HU_TITLE_CHEX;
	}

	// dehacked substitution to get modified level name

	s = s;

	while (*s)
		HUlib_addCharToTextLine(&w_title, *(s++));

	// create the chat widget
	HUlib_initIText(&w_chat,
			HU_INPUTX, HU_INPUTY,
			hu_font,
			HU_FONTSTART, &chat_on);

	// create the inputbuffer widgets
	for (i=0 ; i<MAXPLAYERS ; i++)
		HUlib_initIText(&w_inputbuffer[i], 0, 0, 0, 0, &always_off);

	headsupactive = true;

}

void HU_Drawer(void)
{

	HUlib_drawSText(&w_message);
	HUlib_drawIText(&w_chat);
	if (automapactive)
		HUlib_drawTextLine(&w_title, false);

}

void HU_Erase(void)
{

	HUlib_eraseSText(&w_message);
	HUlib_eraseIText(&w_chat);
	HUlib_eraseTextLine(&w_title);

}

void HU_Ticker(void)
{

	int i, rc;
	char c;

	// tick down message counter if message is up
	if (message_counter && !--message_counter)
	{
		message_on = false;
		message_no_overwrite = false;
	}

	if (showMessages || message_force)
	{
		// display message if necessary
		if (plr->message)
		{
			if (message_force || !message_no_overwrite)
			{
				HUlib_addMessageToSText(&w_message, 0, plr->message);
				plr->message = NULL;

				message_on = true;
				message_counter = HU_MSGTIMEOUT;
				message_no_overwrite = message_force;
				message_force = false;
			}
		}
	}

	// check for incoming chat characters
	if (netgame)
	{
		for (i=0 ; i<MAXPLAYERS; i++)
		{
			if (!players[i].in_game)
				continue;
			if (i != consoleplayer
					&& (c = players[i].cmd.chatchar))
			{
				if (c <= HU_BROADCAST)
					chat_dest[i] = c;
				else
				{
					rc = HUlib_keyInIText(&w_inputbuffer[i], c);
					if (rc && c == KEY_ENTER)
					{
						if (w_inputbuffer[i].l.len
								&& (chat_dest[i] == consoleplayer+1
									|| chat_dest[i] == HU_BROADCAST))
						{
							HUlib_addMessageToSText(&w_message,
									player_names[i],
									w_inputbuffer[i].l.l);

							message_on = true;
							message_no_overwrite = true;
							message_counter = HU_MSGTIMEOUT;

							if ( gamemode == commercial )
								S_StartSound(0, sfx_radio);
							else
								S_StartSound(0, sfx_tink);
						}
						HUlib_resetIText(&w_inputbuffer[i]);
					}
				}
				players[i].cmd.chatchar = 0;
			}
		}
	}

}

#define QUEUESIZE		128

static char	chatchars[QUEUESIZE];
static int	head = 0;
static int	tail = 0;


static void HU_queueChatChar(char c)
{
	if (((head + 1) & (QUEUESIZE-1)) == tail)
	{
		plr->message = HUSTR_MSGU;
	}
	else
	{
		chatchars[head] = c;
		head = (head + 1) & (QUEUESIZE-1);
	}
}

char HU_DequeueChatChar(void)
{
	char c;

	if (head != tail)
	{
		c = chatchars[tail];
		tail = (tail + 1) & (QUEUESIZE-1);
	}
	else
	{
		c = 0;
	}

	return c;
}

/*
   static void StartChatInput(int dest)
   {
   chat_on = true;
   HUlib_resetIText(&w_chat);
   HU_queueChatChar(HU_BROADCAST);

   I_StartTextInput(0, 8, SCREENWIDTH, 16);
   }
 */

static void StopChatInput(void)
{
	chat_on = false;
	I_StopTextInput();
}

boolean HU_Responder(event_t *ev)
{

	static char		lastmessage[HU_MAXLINELENGTH+1];
	char*		macromessage;
	boolean		eatkey = false;
	static boolean	altdown = false;
	unsigned char 	c;
	int			i;
	int			numplayers;

	numplayers = 0;
	for (i=0 ; i<MAXPLAYERS ; i++)
		numplayers += players[i].in_game ? 1 : 0;

	if (ev->data1 == KEY_SHIFT)
	{
		return false;
	}
	else if (ev->data1 == KEY_ALT)
	{
		altdown = ev->type == ev_keydown;
		return false;
	}

	if (ev->type != ev_keydown)
		return false;

	if (!chat_on)
	{
		if (ev->data1 == key_msg_refresh)
		{
			message_on = true;
			message_counter = HU_MSGTIMEOUT;
			eatkey = true;
		}
	}
	else
	{
		// send a macro
		if (altdown)
		{
			c = ev->data1 - '0';
			if (c > 9)
				return false;
			// fprintf(stderr, "got here\n");
			macromessage = chat_macros[c];

			// kill last message with a '\n'
			HU_queueChatChar(KEY_ENTER); // DEBUG!!!

			// send the macro message
			while (*macromessage)
				HU_queueChatChar(*macromessage++);
			HU_queueChatChar(KEY_ENTER);

			// leave chat mode and notify that it was sent
			StopChatInput();
			M_StringCopy(lastmessage, chat_macros[c], sizeof(lastmessage));
			plr->message = lastmessage;
			eatkey = true;
		}
		else
		{
			c = ev->data3;

			eatkey = HUlib_keyInIText(&w_chat, c);
			if (eatkey)
			{
				// static unsigned char buf[20]; // DEBUG
				HU_queueChatChar(c);

				// M_snprintf(buf, sizeof(buf), "KEY: %d => %d", ev->data1, c);
				//        plr->message = buf;
			}
			if (c == KEY_ENTER)
			{
				StopChatInput();
				if (w_chat.l.len)
				{
					M_StringCopy(lastmessage, w_chat.l.l, sizeof(lastmessage));
					plr->message = lastmessage;
				}
			}
			else if (c == KEY_ESCAPE)
			{
				StopChatInput();
			}
		}
	}

	return eatkey;
}
