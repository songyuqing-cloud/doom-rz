;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

zero-var players [MAXPLAYERS]player_t

#private

; Index of the special effects (INVUL inverse) map.
const BRIGHT_COLORMAP  = 1
const INVERSE_COLORMAP = 32

;
; Movement.
;

const ANG5 = (ANG90 / 18)

; 16 pixels of bob
const MAXBOB = (16 * FRACUNIT)

zero-var onground bool

;
; P_Thrust
; Moves the given origin along a given angle.
;
fun P_Thrust (p ^ player_t angle angle_t move fixed_t)
	mo = [p .mo]

	angle = ishr angle ANGLEFINESHIFT

	dx = FixedMul move [finecosine angle]
	dy = FixedMul move [finesine   angle]

	[mo .momx] = iadd [mo .momx] dx
	[mo .momy] = iadd [mo .momy] dy
end

;
; P_CalcHeight
; Calculate the walking / running height adjustment
;
fun P_CalcHeight (p ^player_t)
	mo = [p .mo]

	; Regular movement bobbing
	; (needs to be calculated for gun swing even if not on ground)

	momx_sqr = FixedMul [mo .momx] [mo .momx]
	momy_sqr = FixedMul [mo .momy] [mo .momy]

	bob = iadd momx_sqr momy_sqr
	bob = ishr bob 2
	bob = imin bob MAXBOB

	[p .bob] = bob

	if not [onground]
		[p .viewz] = iadd [mo .z] VIEWHEIGHT
		return
	endif

	; move viewheight
	if eq? [p .playerstate] PST_LIVE
		[p .viewheight] = iadd [p .viewheight] [p .deltaviewheight]

		if gt? [p .viewheight] VIEWHEIGHT
			[p .viewheight] = VIEWHEIGHT
			[p .deltaviewheight] = 0
		endif

		if lt? [p .viewheight] (VIEWHEIGHT / 2)
			[p .viewheight] = (VIEWHEIGHT / 2)
			if le? [p .deltaviewheight] 0
				[p .deltaviewheight] = 1
			endif
		endif

		if some? [p .deltaviewheight]
			[p .deltaviewheight] = iadd [p .deltaviewheight] (FRACUNIT / 4)
			if zero? [p .deltaviewheight]
				[p .deltaviewheight] = 1
			endif
		endif
	endif

	angle s32 = imul [leveltime] (FINEANGLES / 20)
	angle     = iand angle FINEMASK

	bob = FixedMul [p .bob] [finesine angle]
	bob = ishr bob 1

	new_viewz  = iadd [mo .z] [p .viewheight] bob
	max_viewz  = isub [mo .ceilz] (4 * FRACUNIT)
	[p .viewz] = imin new_viewz max_viewz
end

;
; P_MovePlayer
;
fun P_MovePlayer (p ^player_t)
	mo  = [p .mo]
	cmd = [addr-of p .cmd]

	angle = conv angle_t [cmd .angleturn]
	angle = ishl angle FRACBITS

	[mo .angle] = iadd [mo .angle] angle

	; do not let the player control movement if not onground.
	[onground] = le? [mo .z] [mo .floorz]

	some_fwd  = some? [cmd .forwardmove]
	some_side = some? [cmd .sidemove]

	if [onground]
		if some_fwd
			angle = [mo .angle]
			move  = conv fixed_t [cmd .forwardmove]
			move  = imul move 2048

			P_Thrust p angle move
		endif

		if some_side
			angle = isub [mo .angle] ANG90
			move  = conv fixed_t [cmd .sidemove]
			move  = imul move 2048

			P_Thrust p angle move
		endif
	endif

	if and some_fwd some_side
		idle_state = [addr-of states S_PLAY]

		if eq? [mo .state] idle_state
			P_SetMobjState mo S_PLAY_RUN1
		endif
	endif
end

;
; P_DeathThink
;
; Fall on your face when dying.
; Decrease POV height to floor height.
;
fun P_DeathThink (p ^player_t)
	mo = [p .mo]

	P_MovePsprites p

	; fall to the ground
	if gt? [p .viewheight] (6 * FRACUNIT)
		[p .viewheight] = isub [p .viewheight] FRACUNIT
	endif

	[p .viewheight] = imax [p .viewheight] (6 * FRACUNIT)
	[p .deltaviewheight] = 0

	[onground] = le? [mo .z] [mo .floorz]

	P_CalcHeight p

	attacker = [p .attacker]

	if and (ref? attacker) (ne? attacker mo)
		angle = R_PointToAngle2 [mo .x] [mo .y] [attacker .x] [attacker .y]
		delta = isub angle [mo .angle]

		near_zero = lt? delta ANG5
		near_zero = or near_zero (gt? delta (ANG_MAX - ANG5))

		if near_zero
			[mo .angle] = angle

			; Looking at killer, so fade damage flash down.
			jump fade_damage
		endif

		if lt? delta ANG180
			[mo .angle] = iadd [mo .angle] ANG5
		else
			[mo .angle] = isub [mo .angle] ANG5
		endif
	else
		fade_damage:
		if pos? [p .damagecount]
			[p .damagecount] = isub [p .damagecount] 1
		endif
	endif

	if some? (iand [p .cmd .buttons] BT_USE)
		[p .playerstate] = PST_REBORN
	endif
end

#public

;
; P_PlayerThink
;
fun P_PlayerThink (p ^player_t)
	mo  = [p .mo]
	cmd = [addr-of p .cmd]

	; fixme: do this in the cheat code
	if some? (iand [p .cheats] CF_NOCLIP)
		[mo .flags] = ior [mo .flags] MF_NOCLIP
	else
		[mo .flags] = iand [mo .flags] (~ MF_NOCLIP)
	endif

	; chainsaw run forward
	if some? (iand [mo .flags] MF_JUSTATTACKED)
		[cmd .angleturn]   = 0
		[cmd .forwardmove] = (0xc800 / 512)
		[cmd .sidemove]    = 0

		[mo .flags] = iand [mo .flags] (~ MF_JUSTATTACKED)
	endif

	if eq? [p .playerstate] PST_DEAD
		P_DeathThink p
		return
	endif

	; check references
	if ref? [p .attacker]
		if P_MobjIsRemoved [p .attacker]
			P_SetAttacker p NULL
		endif
	endif

	; Move around.
	; Reactiontime is used to prevent movement after a teleport.
	if pos? [mo .reactiontime]
		[mo .reactiontime] = isub [mo .reactiontime] 1
	else
		P_MovePlayer p
	endif

	P_CalcHeight p
	P_PlayerInSpecialSector p

	; Check for weapon change.

	; A special event has no other buttons.
	if some? (iand [cmd .buttons] BT_SPECIAL)
		[cmd .buttons] = 0
	endif

	if some? (iand [cmd .buttons] BT_CHANGE)
		; The actual changing of the weapon is done
		;   when the weapon psprite can do it
		;   (read: not in the middle of an attack).
		w = conv weapontype_t [cmd .buttons]
		w = iand w BT_CHANGE_MASK
		w = ishr w BT_CHANGE_SHIFT

		; prefer chainsaw if we have it, unless we have berserk powerup
		if eq? w wp_fist
			if some? [p .owned wp_chainsaw]
				already = eq? [p .ready] wp_chainsaw
				berserk = pos? [p .powers pw_strength]

				if not (and already berserk)
					w = wp_chainsaw
				endif
			endif
		endif

		; prefer the double shotgun if we have it
		if eq? w wp_shotgun
			if eq? [gamemode] commercial
				if some? [p .owned wp_supershotgun]
					if ne? [p .ready] wp_supershotgun
						w = wp_supershotgun
					endif
				endif
			endif
		endif

		is_big = and (eq? w wp_plasma) (eq? w wp_bfg)
		owned  = some? [p .owned w]

		if and is_big (eq? [gamemode] shareware)
			; do not go to plasma or BFG in shareware, even if cheated.
			owned = FALSE
		endif

		if and (ne? [p .ready] w) owned
			[p .pending] = w
		endif
	endif

	; check for use key
	if some? (iand [cmd .buttons] BT_USE)
		if not [p .usedown]
			P_UseLines p
			[p .usedown] = TRUE
		endif
	else
		[p .usedown] = FALSE
	endif

	; cycle psprites
	P_MovePsprites p

	; Counters, time dependend power ups...

	; strength counts up to diminish fade.
	if pos? [p .powers pw_strength]
		[p .powers pw_strength] = iadd [p .powers pw_strength] 1
	endif

	if pos? [p .powers pw_invulnerability]
		[p .powers pw_invulnerability] = isub [p .powers pw_invulnerability] 1
	endif

	if pos? [p .powers pw_invisibility]
		[p .powers pw_invisibility] = isub [p .powers pw_invisibility] 1

		if zero? [p .powers pw_invisibility]
			[mo .flags] = iand [mo .flags] (~ MF_SHADOW)
		endif
	endif

	if pos? [p .powers pw_infrared]
		[p .powers pw_infrared] = isub [p .powers pw_infrared] 1
	endif

	if pos? [p .powers pw_ironfeet]
		[p .powers pw_ironfeet] = isub [p .powers pw_ironfeet] 1
	endif

	if pos? [p .damagecount]
		[p .damagecount] = isub [p .damagecount] 1
	endif

	if pos? [p .bonuscount]
		[p .bonuscount] = isub [p .bonuscount] 1
	endif

	; handling colormaps.
	[p .fixedcolormap] = 0

	invuln = [p .powers pw_invulnerability]
	infra  = [p .powers pw_infrared]

	if pos? invuln
		high  = gt? invuln (4 * 32)
		blink = some? (iand invuln 8)

		if or high blink
			[p .fixedcolormap] = INVERSE_COLORMAP
		endif

	elif pos? infra
		high  = gt? infra (4 * 32)
		blink = some? (iand infra 8)

		if or high blink
			[p .fixedcolormap] = BRIGHT_COLORMAP
		endif
	endif
end
