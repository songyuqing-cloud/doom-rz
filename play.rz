;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


; The data sampled per tick (single player)
; and transmitted to other peers (multiplayer).
; Mainly movements/button commands per game tick,
; plus a checksum for internal state consistency.

type ticcmd_t struct
	.forwardmove   s8     ;  *2048 for move
	.sidemove      s8     ;  *2048 for move
	.angleturn     s16    ;  <<16 for angle delta

	.chatchar      uchar
	.buttons       u8
	.consistancy   u8     ;  checks for net game
	._pad          u8
end

;
; Button/action code definitions.
;
const BT_ATTACK        = 1    ; Press "Fire".
const BT_USE           = 2    ; Use button, open doors and activate switches.

const BT_CHANGE        = 4    ; Flag: weapon change pending.
const BT_CHANGE_MASK   = 0x38
const BT_CHANGE_SHIFT  = 3

const BT_SPECIAL       = 0x80 ; Flag: game events (not really buttons)
const BT_SPECIAL_MASK  = 3    ; four possibilities, two in use

const BTS_PAUSE        = 1  ; Pause the game.
const BTS_SAVEGAME     = 2  ; Save the game at given slot.
const BTS_SAVEGAME_SHIFT    = 2
const BTS_SAVEGAME_MASK     = (7 << BTS_SAVEGAME_SHIFT)

;;
;; THINGS
;;

; Thing definition, position, orientation and type,
; plus skill/visibility flags and attributes.
type mapthing_t struct
	.x        s16
	.y        s16
	.angle    u16
	.type     s16
	.options  u16
end

type mobj_t struct
	.thinker    thinker_t

	; current position
	.x          fixed_t
	.y          fixed_t
	.z          fixed_t
	.angle      angle_t   ; orientation

	.info      ^mobjinfo_t   ; entry in mobjinfo[]

	.subsector ^subsector_t

	; links in sector (if needed)
	.snext     ^mobj_t
	.sprev     ^mobj_t

	; links in BLOCKMAP blocks (if needed).
	.bnext     ^mobj_t
	.bprev     ^mobj_t

	.player    ^player_t ; additional info record for player avatars only.
	.target    ^mobj_t   ; thing being chased/attacked, originator for missiles.
	.tracer    ^mobj_t   ; used for revenant missiles and archvile fire.

	.state     ^state_t  ; current state and tic counter
	.tics       s32      ;

	.sprite     spritenum_t  ; used to find patch_t and flip value
	.frame      s32      ; can be ORed with FF_FULLBRIGHT

	.floorz     fixed_t  ; the closest interval over all contacted Sectors.
	.ceilz      fixed_t  ;

	.radius     fixed_t  ; size, for movement checking.
	.height     fixed_t  ;

	.momx       fixed_t  ; momentum.
	.momy       fixed_t  ;
	.momz       fixed_t  ;

	.type       mobjtype_t
	.flags      mobjflag_t
	.health     s32

	; monster movement generation (zig-zagging).
	.movedir    s32  ; DI_XXX value, 0-8
	.movecount  s32  ; when 0, select a new dir

	.reactiontime  s32  ; if non 0, don't attack yet.

	.threshold  s32  ; if > 0, the target will be chased no matter what
	.lastlook   s32  ; player number last looked for.
	.validcount s32  ; if == validcount global, already checked.

	.spawnpoint mapthing_t  ; nightmare respawn point
	.ref_count  s32         ; andrewj: number of external references
	._pad2      [12]u8
end

const FF_FULLBRIGHT = 0x8000
const FF_FRAMEMASK  = 0x7fff

; Miscellaneous mobj flags
type mobjflag_t s32

const MF_SPECIAL    = 1   ; Call P_SpecialThing when touched.
const MF_SOLID      = 2   ; Blocking.
const MF_SHOOTABLE  = 4   ; Can be hit.
const MF_NOSECTOR   = 8   ; Don't use the sector links (invisible but touchable).
const MF_NOBLOCKMAP = 16  ; Don't use the blocklinks (inert but displayable)
const MF_AMBUSH     = 32  ; Not to be activated by sound, deaf monster.
const MF_JUSTHIT    = 64  ; Will try to attack right back.

const MF_JUSTATTACKED = 0x80   ; Will take at least one step before attacking.
const MF_SPAWNCEILING = 0x100  ; On spawning hang from ceiling instead of stand on floor.
const MF_NOGRAVITY    = 0x200  ; Don't apply gravity (every tic), i.e. object will float.

const MF_DROPOFF    = 0x400    ; This allows jumps from high places.
const MF_PICKUP     = 0x800    ; For players, will pick up items.
const MF_NOCLIP     = 0x1000   ; Player cheat, pass through anything.
const MF_SLIDE      = 0x2000   ; Player: keep info about sliding along walls.
const MF_FLOAT      = 0x4000   ; Allow moves to any height.  For floaters like cacodemons.
const MF_TELEPORT   = 0x8000   ; Don't care about heights when crossing lines.
const MF_MISSILE    = 0x10000  ; Don't hit same species, explode on block.
const MF_DROPPED    = 0x20000  ; Dropped by a demon, not level spawned.
const MF_SHADOW     = 0x40000  ; Use fuzzy draw (shadow demons or spectres), also player.
const MF_NOBLOOD    = 0x80000  ; Flag: don't bleed when shot (use puff).
const MF_CORPSE     = 0x100000 ; Don't stop moving halfway off a step.
const MF_INFLOAT    = 0x200000 ; Floating to a height for a move.

const MF_COUNTKILL   = 0x400000  ; Count this enemy towards intermission kill total.
const MF_COUNTITEM   = 0x800000  ; Count this item towards intermission item total.
const MF_SKULLFLY    = 0x1000000 ; Special handling: skull in flight.
const MF_NOTDMATCH   = 0x2000000 ; Don't spawn this object in death match mode
const MF_TRANSLATION = 0xc000000 ; Color translation bits for Player sprites in multiplayer.
const MF_TRANS_SHIFT = 26

;;
;; PLAYERS
;;

const MAX_DM_STARTS = 10

type player_t struct
	.mo    ^mobj_t
	.cmd    ticcmd_t

	.message   ^uchar    ; hint messages.
	.attacker  ^mobj_t   ; who did damage (NULL for floors/ceilings).

	.index          s32  ; 0..MAXPLAYERS-1
	.playerstate    playerstate_t

	; Determine POV
	.viewz            fixed_t  ; focal origin above mo.z
	.viewheight       fixed_t  ; base height above floor for viewz
	.deltaviewheight  fixed_t  ; bob/squat speed
	.bob              fixed_t  ; bounded/scaled total momentum

	.health       s32
	.armorpoints  s32
	.armortype    s32   ; armor type is 0-2.

	; Power ups. invinc and invis are tic counters.
	.powers[NUMPOWERS]  s32
	.cards[NUMCARDS]    bool
	.backpack           bool
	.didsecret          bool  ; true if secret level has been done.

	.frags[MAXPLAYERS]  s32   ; kills of other players.

	.ready      weapontype_t  ; weapon in use
	.pending    weapontype_t  ; wp_nochange if not changing.

	.owned[NUMWEAPONS]  s32   ; weapons owned.  status bar code needs s32 (not bool)
	.ammo[NUMAMMO]      s32
	.maxammo[NUMAMMO]   s32

	.attackdown     bool  ; true if button down last tic.
	.usedown        bool
	.refire         bool  ; refired shots are less accurate.
	.in_game        bool

	.cheats         cheat_t  ; bit flags, for cheats and debug.

	; Intermission stats.
	.killcount      s32
	.itemcount      s32
	.secretcount    s32

	.damagecount    s32  ; for screen flashing (red or bright).
	.bonuscount     s32
	.extralight     s32  ; so gun flashes light up areas.

	.fixedcolormap  s32  ;  for invincibility and light goggles

	.pad4 [4]u8  ; bring size of player_t to multiple of 16

	; Overlay view sprites (gun, etc).
	.psprites[NUMPSPRITES]  pspdef_t
end

; Player states.
type playerstate_t s32

const PST_LIVE   = 0  ; Playing or camping.
const PST_DEAD   = 1  ; Dead on the ground, view follows killer.
const PST_REBORN = 2  ; Ready to restart/respawn

; Player internal flags, for cheats and debug.
type cheat_t s32

const CF_NOCLIP  = 1  ; No clipping, walk through barriers.
const CF_GODMODE = 2  ; No damage, no health loss.

const ps_weapon = 0
const ps_flash  = 1
const NUMPSPRITES = 2

type pspdef_t struct
	.state  ^state_t  ; a NULL state means not active
	.tics    s32
	._pad    s32
	.sx      fixed_t
	.sy      fixed_t
end

const VIEWHEIGHT = (41 * FRACUNIT)

;
; INTERMISSION
; Structure passed e.g. to WI_Start()
;
type wbstartstruct_t struct
	.episode  s32    ; episode # (0-2)
	.last     s32    ; previous and next levels, origin 0
	.next     s32    ;

	.did_secret s32  ; if true, splash the secret level

	.max_kills  s32
	.max_items  s32
	.max_secret s32
	.max_frags  s32

	.partime    s32  ; the par time
	.pnum       s32  ; index of shown player in game

	.plyr [MAXPLAYERS]wbplayer_t
end

type wbplayer_t struct
	.in_game  s32

	.kills    s32  ; Player stats, kills, collected items etc.
	.items    s32  ;
	.secret   s32  ;
	.time     s32  ;
	.frags    [4]s32
end

;  p_info

type state_t struct
	.sprite  spritenum_t
	.bright  s32
	.frame   s32
	.tics    s32

	.action  actionf_t  ; function pointer
	.next    statenum_t
	.misc1   s16
	.misc2   s16
end

type mobjinfo_t struct
	.doomednum      s32
	.spawnstate     s32
	.spawnhealth    s32
	.seestate       s32
	.seesound       s32
	.reactiontime   s32
	.attacksound    s32
	.painstate      s32

	.painchance     s32
	.painsound      s32
	.meleestate     s32
	.missilestate   s32
	.deathstate     s32
	.xdeathstate    s32
	.deathsound     s32
	.speed          s32

	.radius         s32
	.height         s32
	.mass           s32
	.damage         s32
	.activesound    s32
	.flags          s32
	.raisestate     s32
	._pad           s32
end


;  p_weapon

type weaponinfo_t struct
	.ammo        ammotype_t

	.upstate     s32
	.downstate   s32
	.readystate  s32
	.atkstate    s32
	.flashstate  s32
end

; DeHackEd constants

const DEH_DEFAULT_INITIAL_HEALTH     = 100
const DEH_DEFAULT_INITIAL_BULLETS    = 50
const DEH_DEFAULT_MAX_HEALTH         = 200
const DEH_DEFAULT_MAX_ARMOR          = 200
const DEH_DEFAULT_GREEN_ARMOR_CLASS  = 1
const DEH_DEFAULT_BLUE_ARMOR_CLASS   = 2
const DEH_DEFAULT_MAX_SOULSPHERE     = 200
const DEH_DEFAULT_SOULSPHERE_HEALTH  = 100
const DEH_DEFAULT_MEGASPHERE_HEALTH  = 200
const DEH_DEFAULT_GOD_MODE_HEALTH    = 100
const DEH_DEFAULT_IDFA_ARMOR         = 200
const DEH_DEFAULT_IDFA_ARMOR_CLASS   = 2
const DEH_DEFAULT_IDKFA_ARMOR        = 200
const DEH_DEFAULT_IDKFA_ARMOR_CLASS  = 2
const DEH_DEFAULT_BFG_CELLS_PER_SHOT = 40
const DEH_DEFAULT_SPECIES_INFIGHTING = 0

const deh_initial_health      = DEH_DEFAULT_INITIAL_HEALTH
const deh_initial_bullets     = DEH_DEFAULT_INITIAL_BULLETS
const deh_max_health          = DEH_DEFAULT_MAX_HEALTH
const deh_max_armor           = DEH_DEFAULT_MAX_ARMOR
const deh_green_armor_class   = DEH_DEFAULT_GREEN_ARMOR_CLASS
const deh_blue_armor_class    = DEH_DEFAULT_BLUE_ARMOR_CLASS
const deh_max_soulsphere      = DEH_DEFAULT_MAX_SOULSPHERE
const deh_soulsphere_health   = DEH_DEFAULT_SOULSPHERE_HEALTH
const deh_megasphere_health   = DEH_DEFAULT_MEGASPHERE_HEALTH
const deh_god_mode_health     = DEH_DEFAULT_GOD_MODE_HEALTH
const deh_idfa_armor          = DEH_DEFAULT_IDFA_ARMOR
const deh_idfa_armor_class    = DEH_DEFAULT_IDFA_ARMOR_CLASS
const deh_idkfa_armor         = DEH_DEFAULT_IDKFA_ARMOR
const deh_idkfa_armor_class   = DEH_DEFAULT_IDKFA_ARMOR_CLASS
const deh_bfg_cells_per_shot  = DEH_DEFAULT_BFG_CELLS_PER_SHOT
const deh_species_infighting  = DEH_DEFAULT_SPECIES_INFIGHTING

;;
;; INTERNAL MAP TYPES
;; used by play and refresh
;;

;
; Move clipping aid for LineDefs.
;
type slopetype_t s32

const ST_HORIZONTAL = 0
const ST_VERTICAL   = 1
const ST_POSITIVE   = 2
const ST_NEGATIVE   = 3

;
; Your plain vanilla vertex.
; Note: transformed values not buffered locally,
; like some DOOM-alikes ("wt", "WebView") did.
;
type vertex_t struct
	.x  fixed_t
	.y  fixed_t
end

;
; The SECTORS record, at runtime.
; Stores things/mobjs.
;
type sector_t struct
	; origin for any sounds played by the sector
	.soundorg    soundmobj_t

	.floorh      fixed_t
	.ceilh       fixed_t

	.floorpic    s32
	.ceilpic     s32

	.light       s16
	.special     s16
	.tag         s16

	.soundtraversed  s16  ; 0 = untraversed, 1, 2 = lines crossed
	.blockbox[4]     s32  ; mapblock bounding box for height changes
	.validcount      s32  ; if == validcount global, already checked

	.linecount       s32
	.lines          ^[0]^line_t  ; [linecount] size

	.soundtarget    ^mobj_t   ; thing that made a sound (or null)
	.thinglist      ^mobj_t   ; list of mobjs in sector
	.specialdata    ^raw-mem  ; thinker_t for reversable actions
end

;
; The SideDef.
;
type side_t struct
	.sector   ^sector_t  ; Sector the SideDef is facing.

	.x_offset  fixed_t   ; add this to the calculated texture column
	.y_offset  fixed_t   ; add this to the calculated texture top

	.top       s32  ; Texture indices (no names here)
	.bottom    s32  ;
	.mid       s32  ;

	._pad      s32
end

;
; The LineDef.
;
const ML_BLOCKING       =   1  ; Solid, is an obstacle.
const ML_BLOCKMONSTERS  =   2  ; Blocks monsters only.
const ML_TWOSIDED       =   4  ; Backside will not be present at all if not two sided.
const ML_DONTPEGTOP     =   8  ; upper texture unpegged
const ML_DONTPEGBOTTOM  =  16  ; lower texture unpegged
const ML_SECRET         =  32  ; In AutoMap: don't map as two sided: IT'S A SECRET!
const ML_SOUNDBLOCK     =  64  ; Sound rendering: don't let sound cross two of these.
const ML_DONTDRAW       = 128  ; Don't draw on the automap at all.
const ML_MAPPED         = 256  ; Set if already seen, thus drawn in automap.

type line_t struct
	; Vertices, from v1 to v2.
	.v1      ^vertex_t
	.v2      ^vertex_t

	; Front and back sector.
	.front   ^sector_t
	.back    ^sector_t

	; Precalculated v2 - v1 for side checking.
	.dx       fixed_t
	.dy       fixed_t

	; Animation related.
	.flags    u16
	.special  s16
	.tag      s16
	._pad     s16

	.sidenum  [2]s32   ; references SideDef, or -1 for none
	.bbox[4]  fixed_t  ; another bounding box, for the extent of the LineDef.

	.slopetype   slopetype_t  ; to aid move clipping.
	.validcount  s32          ; if == validcount globalalready checked

	._pad2    [8]u8
end

;
; A SubSector.
;
; References a Sector (always a part of one).
; Basically, this is a list of LineSegs,
;   indicating the visible walls that define
;   (all or some) sides of a convex BSP leaf.
;
type subsector_t struct
	.sector     ^sector_t
	.numlines    s32
	.firstline   s32
end

;
; The LineSeg.
;
type seg_t struct
	.v1       ^vertex_t
	.v2       ^vertex_t

	.offset    fixed_t
	.angle     angle_t

	.sidedef  ^side_t
	.linedef  ^line_t

	; Sector references.
	; Could be retrieved from linedef, too.
	; backsector is NULL for one sided lines.
	.front    ^sector_t
	.back     ^sector_t
end

; Indicate a leaf of the BSP tree.
const NF_SUBSECTOR = 0x8000

;
; BSP node.
;
type node_t struct
	; Partition line.
	.x   fixed_t
	.y   fixed_t
	.dx  fixed_t
	.dy  fixed_t

	; Bounding box for each child.
	.bbox  [2][4]fixed_t

	; Added with NF_SUBSECTOR for a subsector.
	.children  [2]u32
end
