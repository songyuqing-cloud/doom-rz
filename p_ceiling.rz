;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const CEILSPEED = FRACUNIT
const CEILWAIT  = 150

type ceiling_type_e s32

const ce_lowerToFloor        = 0
const ce_lowerAndCrush       = 1
const ce_raiseToHighest      = 2
const ce_crushAndRaise       = 3
const ce_fastCrushAndRaise   = 4
const ce_silentCrushAndRaise = 5

type ceiling_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.type       ceiling_type_e
	.bottom     fixed_t
	.top        fixed_t
	.speed      fixed_t

	.direction  s32  ; 1 = up, -1 = down, 0 = in stasis
	.old_dir    s32  ;
	.tag        s16
	.crush      bool
	.silent     bool
	._pad       [4]u8
end


;
; T_MoveCeiling
;
fun T_MoveCeiling (C ^ceiling_t)
	sec  = [C .sector]
	kind = [C .type]

	t = iand [leveltime] 7

	; UP
	if eq? [C .direction] +1
		res = T_MoveCeilingPlane sec [C .speed] [C .top] FALSE [C .direction]

		if zero? t
			if not [C .silent]
				S_StartSound [addr-of sec .soundorg] sfx_stnmov
			endif
		endif

		if eq? res RES_pastdest
			if eq? kind ce_raiseToHighest
				P_RemoveActiveCeiling C

			elif eq? kind ce_silentCrushAndRaise
				S_StartSound [addr-of sec .soundorg] sfx_pstop
				[C .direction] = -1

			elif ge? kind ce_crushAndRaise
				[C .direction] = -1
			endif
		endif

		return
	endif

	; DOWN
	if eq? [C .direction] -1
		res = T_MoveCeilingPlane sec [C .speed] [C .bottom] [C .crush] [C .direction]

		if zero? t
			if not [C .silent]
				S_StartSound [addr-of sec .soundorg] sfx_stnmov
			endif
		endif

		if eq? res RES_pastdest
			if le? kind ce_lowerAndCrush
				P_RemoveActiveCeiling C

			elif eq? kind ce_silentCrushAndRaise
				S_StartSound [addr-of sec .soundorg] sfx_pstop
				[C .speed] = CEILSPEED
				[C .direction] = +1

			elif eq? kind ce_crushAndRaise
				[C .speed] = CEILSPEED
				[C .direction] = +1

			elif eq? kind ce_fastCrushAndRaise
				[C .direction] = +1
			endif

		elif eq? res RES_crushed
			; slow down
			cru1 = eq? kind ce_crushAndRaise
			cru2 = eq? kind ce_silentCrushAndRaise
			cru3 = eq? kind ce_lowerAndCrush

			if or cru1 cru2 cru3
				[C .speed] = (CEILSPEED / 8)
			endif
		endif

		return
	endif

	; IN STASIS
end

;
; EV_DoCeiling
; Move a ceiling up/down and all around!
;
fun EV_DoCeiling (ld ^line_t kind ceiling_type_e -> bool)
	did = FALSE

	;  Reactivate in-stasis ceilings...for certain types.
	cru1 = eq? kind ce_crushAndRaise
	cru2 = eq? kind ce_fastCrushAndRaise
	cru3 = eq? kind ce_silentCrushAndRaise

	if or cru1 cru2 cru3
		P_ActivateCeilingInStasis ld
	endif

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		if null? [sec .specialdata]
			; new thinker
			C ^ceiling_t = Z_Calloc ceiling_t.size PU_LEVEL

			[C .thinker .func] = THINK_ceil

			; set parameters, some are adjusted further below
			[C .type]   = kind
			[C .sector] = sec
			[C .top]    = [sec .ceilh]
			[C .bottom] = [sec .floorh]
			[C .tag]    = [sec .tag]
			[C .speed]  = CEILSPEED
			[C .crush]  = or cru1 cru2 cru3
			[C .silent] = eq? kind ce_silentCrushAndRaise
			[C .direction] = -1

			; Note: .crush will be FALSE for ce_lowerAndCrush

			if eq? kind ce_raiseToHighest
				[C .top] = P_FindHighestCeilingSurrounding sec
				[C .direction] = +1

			elif eq? kind ce_fastCrushAndRaise
				[C .speed]  = (CEILSPEED * 2)
			endif

			if ne? kind ce_lowerToFloor
				[C .bottom] = iadd [C .bottom] (8 * FRACUNIT)
			endif

			P_AddActiveCeiling C

			did = TRUE
		endif
	endloop

	return did
end

;
; EV_CeilingCrushStop
; Stop a ceiling from crushing!
;
fun EV_CeilingCrushStop (ld ^line_t -> bool)
	did = FALSE

	th = [thinkercap .next]
	loop until eq? th thinkercap
		if eq? [th .func] THINK_ceil
			C = raw-cast ^ceiling_t th

			if and (eq? [C .tag] [ld .tag]) (some? [C .direction])
				[C .old_dir]   = [C .direction]
				[C .direction] = 0  ; in stasis

				did = TRUE
			endif
		endif
		th = [th .next]
	endloop

	return did
end

;
; Restart a ceiling that's in stasis
;
fun P_ActivateCeilingInStasis (ld ^line_t)
	th = [thinkercap .next]
	loop until eq? th thinkercap
		if eq? [th .func] THINK_ceil
			C = raw-cast ^ceiling_t th

			if and (eq? [C .tag] [ld .tag]) (eq? [C .direction] 0)
				[C .direction] = [C .old_dir]
			endif
		endif
		th = [th .next]
	endloop
end

;
; Add an active ceiling
;
fun P_AddActiveCeiling (C ^ceiling_t)
	[[C .sector] .specialdata] = C
	P_AddThinker (raw-cast C)
end

;
; Remove a ceiling's thinker
;
fun P_RemoveActiveCeiling (C ^ceiling_t)
	[[C .sector] .specialdata] = NULL
	P_RemoveThinker (raw-cast C)
end

;
; Move a ceiling plane and check for crushing
;
fun T_MoveCeilingPlane (sec ^sector_t speed fixed_t dest fixed_t crush bool direction s32 -> result_e)
	if eq? direction -1
		; DOWN
		last  = [sec .ceilh]
		new_h = isub last speed

		if lt? new_h dest
			[sec .ceilh] = dest

			nofit = P_ChangeSector sec crush
			if nofit
				[sec .ceilh] = last
				P_ChangeSector sec crush
			endif

			return RES_pastdest
		endif

		; COULD GET CRUSHED
		[sec .ceilh] = new_h

		nofit = P_ChangeSector sec crush

		if nofit
			if not crush
				[sec .ceilh] = last
				P_ChangeSector sec crush
			endif

			return RES_crushed
		endif

	elif eq? direction +1
		; UP
		last  = [sec .ceilh]
		new_h = iadd last speed

		if gt? new_h dest
			[sec .ceilh] = dest

			nofit = P_ChangeSector sec crush
			if nofit
				[sec .ceilh] = last
				P_ChangeSector sec crush
			endif

			return RES_pastdest
		endif

		[sec .ceilh] = new_h

		; ceiling is going up, don't need to worry about things not fitting
		P_ChangeSector sec crush
	endif

	return RES_ok
end
