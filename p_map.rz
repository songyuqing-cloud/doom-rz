;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard, Andrey Budko
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const MAXRADIUS   = (32 * FRACUNIT)
const SCREENSLOPE = ((200 * FRACUNIT) / 320)

; result of a P_TryMove
type TryMoveInfo struct
	.mo       ^mobj_t
	.bbox[4]   fixed_t
	.flags     s32

	.x  fixed_t
	.y  fixed_t

	.floorz    fixed_t
	.ceilz     fixed_t
	.dropoffz  fixed_t

	; if true, move would be ok if between .floorz and .ceilz
	.floatok   bool
end

zero-var tm TryMoveInfo


; keep track of the line that lowers the ceiling,
; so missiles don't explode against sky hack walls
zero-var ceilingline ^line_t

; keep track of special lines as they are hit,
; but don't process them until the move is proven valid.
; this limit matches vanilla, but we never write beyond it.
const MAXSPECIALCROSS = 8

zero-var spechit  [MAXSPECIALCROSS]^line_t
zero-var numspechit  s32


;;
;; TELEPORT MOVE
;;

;
; PIT_StompThing
;
fun PIT_StompThing (mo ^mobj_t -> bool)
	; don't clip against self
	if eq? mo [tm .mo]
		return TRUE
	endif

	if zero? (iand [mo .flags] MF_SHOOTABLE)
		return TRUE
	endif

	blockdist = iadd [mo .radius] [[tm .mo] .radius]

	dx = iabs (isub [mo .x] [tm .x])
	dy = iabs (isub [mo .y] [tm .y])

	if ge? (imax dx dy) blockdist
		; didn't hit it
		return TRUE
	endif

	; monsters never stomp other things except on the MAP30 boss level,
	; so don't bother visiting any other things / blocks.
	if null? [[tm .mo] .player]
		if ne? [gamemap] 30
			return FALSE
		endif
	endif

	P_DamageMobj mo [tm .mo] [tm .mo] 10000
	return TRUE
end

;
; P_TeleportMove
; kills anything occupying the position.
;
fun P_TeleportMove (mo ^mobj_t x fixed_t y fixed_t -> bool)
	r = [mo .radius]

	[tm .mo]    = mo
	[tm .flags] = [mo .flags]

	[tm .x] = x
	[tm .y] = y

	[tm .bbox BOXTOP]    = iadd y r
	[tm .bbox BOXBOTTOM] = isub y r
	[tm .bbox BOXRIGHT]  = iadd x r
	[tm .bbox BOXLEFT]   = isub x r

	[ceilingline] = NULL
	[numspechit]  = 0
	[validcount]  = iadd [validcount] 1

	; The base floor/ceiling is from the sector that contains the point.
	; Any contacted lines the step closer together will adjust them.

	sub = R_PointInSubsector x y
	sec = [sub .sector]

	[tm .floorz]   = [sec .floorh]
	[tm .ceilz]    = [sec .ceilh]
	[tm .dropoffz] = [sec .floorh]

	; stomp on any things contacted
	bx1 = Block_X (isub [tm .bbox BOXLEFT]   MAXRADIUS)
	by1 = Block_Y (isub [tm .bbox BOXBOTTOM] MAXRADIUS)
	bx2 = Block_X (iadd [tm .bbox BOXRIGHT]  MAXRADIUS)
	by2 = Block_Y (iadd [tm .bbox BOXTOP]    MAXRADIUS)

	stomp_func = fun PIT_StompThing

	bx = bx1
	loop while le? bx bx2
		by = by1
		loop while le? by by2
			ok = P_BlockThingsIterator bx by stomp_func
			if not ok
				return FALSE
			endif
			by = iadd by 1
		endloop
		bx = iadd bx 1
	endloop

	; the move is ok, so link the thing into its new position
	P_UnsetThingPosition mo

	[mo .x] = x
	[mo .y] = y
	[mo .floorz] = [tm .floorz]
	[mo .ceilz]  = [tm .ceilz]

	P_SetThingPosition mo

	return TRUE
end

;;
;; MOVEMENT ITERATOR FUNCTIONS
;;

;
; PIT_CheckLine
; Adjusts tm.floorz and tm.ceilz as lines are contacted
;
fun PIT_CheckLine (ld ^line_t -> bool)
	left  = le? [tm .bbox BOXRIGHT]  [ld .bbox BOXLEFT]
	right = ge? [tm .bbox BOXLEFT]   [ld .bbox BOXRIGHT]
	below = le? [tm .bbox BOXTOP]    [ld .bbox BOXBOTTOM]
	above = ge? [tm .bbox BOXBOTTOM] [ld .bbox BOXTOP]

	if or left right below above
		return TRUE
	endif

	side = P_BoxOnLineSide [addr-of tm .bbox] ld
	if ne? side -1
		return TRUE
	endif

	; A line has been hit

	; The moving thing's destination position will cross the given line.
	; If this should not be allowed, return false.
	; If the line is special, keep track of it
	; to process later if the move is proven ok.
	;
	; NOTE: specials are NOT sorted by order, so two special lines
	; that are only 8 pixels apart could be crossed in either order.

	if null? [ld .back]
		; one sided line
		return FALSE
	endif

	if zero? (iand [[tm .mo] .flags] MF_MISSILE)
		block_all = some? (iand [ld .flags] ML_BLOCKING)
		block_mon = some? (iand [ld .flags] ML_BLOCKMONSTERS)

		if block_all
			return FALSE
		endif

		if block_mon
			if null? [[tm .mo] .player]
				return FALSE
			endif
		endif
	endif

	; set openrange, opentop, openbottom
	P_LineOpening ld

	; adjust floor / ceiling heights
	if lt? [opentop] [tm .ceilz]
		[tm .ceilz]   = [opentop]
		[ceilingline] = ld
	endif

	[tm .floorz]   = imax [tm .floorz] [openbottom]
	[tm .dropoffz] = imin [tm .dropoffz] [lowfloor]

	; if contacted a special line, add it to the list
	if some? [ld .special]
		P_AddSpecHit ld
	endif

	return TRUE
end

fun P_AddSpecHit (ld ^line_t)
	if lt? [numspechit] MAXSPECIALCROSS
		[spechit [numspechit]] = ld
		[numspechit] = iadd [numspechit] 1
	endif
end

;
; PIT_CheckThing
;
fun PIT_CheckThing (mo ^mobj_t -> bool)
	mover = [tm .mo]

	; don't clip against self
	if eq? mo mover
		return TRUE
	endif

	if zero? (iand [mo .flags] (MF_SOLID | (MF_SPECIAL | MF_SHOOTABLE)) )
		return TRUE
	endif

	blockdist = iadd [mo .radius] [mover .radius]

	dx = iabs (isub [mo .x] [tm .x])
	dy = iabs (isub [mo .y] [tm .y])

	if ge? (imax dx dy) blockdist
		; didn't hit it
		return TRUE
	endif

	; check for skulls slamming into things
	if some? (iand [mover .flags] MF_SKULLFLY)
		r = P_Random
		r = iadd (iremt r 8) 1
		damage = imul r [[mover .info] .damage]

		P_DamageMobj mo mover mover damage

		[mover .flags] = iand [mover .flags] (~ MF_SKULLFLY)
		[mover .momx]  = 0
		[mover .momy]  = 0
		[mover .momz]  = 0

		P_SetMobjState mover [[mover .info] .spawnstate]
		return FALSE   ; stop moving
	endif

	nonsolid = zero? (iand [mo .flags] MF_SOLID)

	; missiles can hit other things
	if some? (iand [mover .flags] MF_MISSILE)
		; see if it went over / under
		over  = gt? [mover .z] (iadd [mo .z] [mo .height])
		under = lt? (iadd [mover .z] [mover .height]) [mo .z]

		if or over under
			return TRUE
		endif

		shooter = [mover .target]

		if ref? shooter
			; missiles never hit their originator.
			if eq? mo shooter
				return TRUE
			endif

			; stop the missile without doing explicit damage?
			; no for players, yes for same species.

			type1 = [shooter .type]
			type2 = [mo .type]

			; sdh: Add deh_species_infighting here.  We can override the
			; "monsters of the same species cant hurt each other" behavior
			; through dehacked patches
			infight = conv s32 deh_species_infighting

			if eq? type1 MT_PLAYER
				infight = 1
			endif

			if zero? infight
				; HellKnights and Barons are the same species.
				; Demons and Spectres are too, but never shoot missiles.
				if eq? type1 MT_KNIGHT
					type1 = MT_BRUISER
				endif

				if eq? type2 MT_KNIGHT
					type2 = MT_BRUISER
				endif

				if eq? type1 type2
					return FALSE
				endif
			endif
		endif

		if zero? (iand [mo .flags] MF_SHOOTABLE)
			; didn't do any damage
			return nonsolid
		endif

		; damage / explode
		r = P_Random
		r = iadd (iremt r 8) 1
		damage = imul r [[mover .info] .damage]

		P_DamageMobj mo mover shooter damage

		; don't traverse any more
		return FALSE
	endif

	; check for special pickup
	if some? (iand [tm .flags] MF_PICKUP)
		if some? (iand [mo .flags] MF_SPECIAL)
			; can remove thing
			P_TouchSpecialThing mo mover
		endif
	endif

	return nonsolid
end


#public

;;
;; MOVEMENT CLIPPING
;;

;
; P_CheckPosition
;
; This is purely informative, nothing is modified
; (except things picked up).
;
; in:
;   a mobj_t (can be valid or invalid)
;   a position to be checked
;     (doesn't need to be related to the mobj_t->x,y)
;
; during:
;   special things are touched if MF_PICKUP
;   early out on solid lines?
;
; out:
;   newsubsec
;   tm.floorz
;   tm.ceilz
;   tm.dropoffz
;     the lowest point contacted
;     (monsters won't move to a dropoff)
;   speciallines[]
;   numspeciallines
;
fun P_CheckPosition (mo ^mobj_t x fixed_t y fixed_t -> bool)
	r = [mo .radius]

	[tm .mo]    = mo
	[tm .flags] = [mo .flags]

	[tm .x] = x
	[tm .y] = y

	[tm .bbox BOXLEFT]   = isub x r
	[tm .bbox BOXRIGHT]  = iadd x r
	[tm .bbox BOXBOTTOM] = isub y r
	[tm .bbox BOXTOP]    = iadd y r

	[ceilingline] = NULL
	[numspechit]  = 0
	[validcount]  = iadd [validcount] 1

	; The base floor/ceiling is from the sector that contains the point.
	; Any contacted lines the step closer together will adjust them.

	sub = R_PointInSubsector x y
	sec = [sub .sector]

	[tm .floorz]   = [sec .floorh]
	[tm .ceilz]    = [sec .ceilh]
	[tm .dropoffz] = [sec .floorh]

	if some? (iand [tm .flags] MF_NOCLIP)
		return TRUE
	endif

	; Check things first, possibly picking things up.
	; The bounding box is extended by MAXRADIUS
	; because mobj_ts are grouped into mapblocks
	; based on their origin point, and can overlap
	; into adjacent blocks by up to MAXRADIUS units.

	bx1 = Block_X (isub [tm .bbox BOXLEFT]   MAXRADIUS)
	by1 = Block_Y (isub [tm .bbox BOXBOTTOM] MAXRADIUS)
	bx2 = Block_X (iadd [tm .bbox BOXRIGHT]  MAXRADIUS)
	by2 = Block_Y (iadd [tm .bbox BOXTOP]    MAXRADIUS)

	thing_func = fun PIT_CheckThing

	bx = bx1
	loop while le? bx bx2
		by = by1
		loop while le? by by2
			ok = P_BlockThingsIterator bx by thing_func
			if not ok
				return FALSE
			endif
			by = iadd by 1
		endloop
		bx = iadd bx 1
	endloop

	; check lines now...

	bx1 = Block_X [tm .bbox BOXLEFT]
	by1 = Block_Y [tm .bbox BOXBOTTOM]
	bx2 = Block_X [tm .bbox BOXRIGHT]
	by2 = Block_Y [tm .bbox BOXTOP]

	line_func = fun PIT_CheckLine

	bx = bx1
	loop while le? bx bx2
		by = by1
		loop while le? by by2
			ok = P_BlockLinesIterator bx by line_func
			if not ok
				return FALSE
			endif
			by = iadd by 1
		endloop
		bx = iadd bx 1
	endloop

	return TRUE
end

#private

;
; P_TryMove
;
; Attempt to move to a new position,
; crossing special lines unless MF_TELEPORT is set.
;
fun P_TryMove (mo ^mobj_t x fixed_t y fixed_t -> bool)
	[tm .floatok] = FALSE

	ok = P_CheckPosition mo x y
	if not ok
		; solid wall or thing
		return FALSE
	endif

	if zero? (iand [mo .flags] MF_NOCLIP)
		room = isub [tm .ceilz] [tm .floorz]

		if lt? room [mo .height]
			; doesn't fit
			return FALSE
		endif

		[tm .floatok] = TRUE

		if zero? (iand [mo .flags] MF_TELEPORT)
			top  = iadd [mo .z] [mo .height]
			step = isub [tm .floorz] [mo .z]

			if gt? top [tm .ceilz]
				; mobj must lower itself to fit
				return FALSE
			endif

			if gt? step (24 * FRACUNIT)
				; too big a step up
				return FALSE
			endif
		endif

		if zero? (iand [mo .flags] (MF_DROPOFF | MF_FLOAT))
			drop = isub [tm .floorz] [tm .dropoffz]

			if gt? drop (24 * FRACUNIT)
				; don't stand over a dropoff
				return FALSE
			endif
		endif
	endif

	; the move is ok,
	; so link the thing into its new position.

	P_UnsetThingPosition mo

	oldx = [mo .x]
	oldy = [mo .y]

	[mo .x] = x
	[mo .y] = y
	[mo .floorz] = [tm .floorz]
	[mo .ceilz]  = [tm .ceilz]

	P_SetThingPosition mo

	; if any special lines were hit, do the effect
	if zero? (iand [mo .flags] (MF_TELEPORT | MF_NOCLIP))
		P_ExamineSpecHits mo oldx oldy
	endif

	return TRUE
end

fun P_ExamineSpecHits (mo ^mobj_t oldx fixed_t oldy fixed_t)
	loop until zero? [numspechit]
		[numspechit] = isub [numspechit] 1

		; see if the line was crossed
		ld = [spechit [numspechit]]

		if some? [ld .special]
			s1 = P_PointOnLineSide oldx oldy ld
			s2 = P_PointOnLineSide [mo .x] [mo .y] ld

			if ne? s1 s2
				P_CrossSpecialLine ld s1 mo
			endif
		endif
	endloop
end

;
; P_ThingHeightClip
;
; Takes a valid thing and adjusts the thing->floorz,
; thing->ceilz, and possibly thing->z.
;
; Called for all nearby monsters whenever a sector changes height.
; If the thing doesn't fit, the z will be set to the lowest value
; and false will be returned.
;
fun P_ThingHeightClip (mo ^mobj_t -> bool)
	onfloor = eq? [mo .z] [mo .floorz]

	P_CheckPosition mo [mo .x] [mo .y]

	[mo .floorz] = [tm .floorz]
	[mo .ceilz]  = [tm .ceilz]

	if onfloor
		; walking monsters rise and fall with the floor
		[mo .z] = [mo .floorz]
	else
		; don't adjust a floating monster unless forced to
		top = iadd [mo .z] [mo .height]
		if gt? top [mo .ceilz]
			[mo .z] = isub [mo .ceilz] [mo .height]
		endif
	endif

	room = isub [mo .ceilz] [mo .floorz]
	if lt? room [mo .height]
		return FALSE
	endif

	return TRUE
end


;;
;; SLIDE MOVE
;; Allows the player to slide along any angled walls.
;;

type SlideInfo struct
	.mo        ^mobj_t
	.bestline  ^line_t
	.bestfrac   fixed_t
	.xmove      fixed_t
	.ymove      fixed_t
end

zero-var sli SlideInfo

;
; P_HitSlideLine
;
; Adjusts the xmove / ymove so that the next move
; will slide along the wall.
;
fun P_HitSlideLine (ld ^line_t)
	if eq? [ld .slopetype] ST_HORIZONTAL
		[sli .ymove] = 0
		return
	endif

	if eq? [ld .slopetype] ST_VERTICAL
		[sli .xmove] = 0
		return
	endif

	mo   = [sli .mo]
	side = P_PointOnLineSide [mo .x] [mo .y] ld

	lineangle = R_PointToAngle2 0 0 [ld .dx] [ld .dy]
	moveangle = R_PointToAngle2 0 0 [sli .xmove] [sli .ymove]

	if eq? side 1
		lineangle = iadd lineangle ANG180
	endif

	delta = isub moveangle lineangle

	if gt? delta ANG180
		; andrewj: this probbably should have been: delta = (0 - delta)
		delta = iadd delta ANG180
	endif

	lineangle = ishr lineangle ANGLEFINESHIFT
	delta     = ishr delta     ANGLEFINESHIFT

	; reduce overall momentum to account for angle between the desired
	; movement direction and the hit line.  e.g. when the angle difference
	; is exactly 90 degrees, the player should come to a complete stop.

	curlen = P_ApproxDistance [sli .xmove] [sli .ymove]
	newlen = FixedMul curlen [finecosine delta]

	[sli .xmove] = FixedMul newlen [finecosine lineangle]
	[sli .ymove] = FixedMul newlen [finesine   lineangle]
end

;
; PTR_SlideTraverse
;
fun PTR_SlideTraverse (incpt ^intercept_t -> bool)
	ld = [incpt .obj .line]
	mo = [sli .mo]

	if zero? (iand [ld .flags] ML_TWOSIDED)
		side = P_PointOnLineSide [mo .x] [mo .y] ld

		; hit the front side?
		jump blocking if zero? side

		; impossible to hit back side of a one-sided line
		return TRUE
	endif

	; set openrange, opentop, openbottom
	P_LineOpening ld

	; doesn't fit?
	jump blocking if lt? [openrange] [mo .height]

	; mobj is too high?
	top = iadd [mo .z] [mo .height]
	jump blocking if gt? top [opentop]

	; too big a step up?
	step = isub [openbottom] [mo .z]
	jump blocking if gt? step (24 * FRACUNIT)

	; this line doesn't block movement
	return TRUE

blocking:
	; the line does block movement.
	; see if it is closer than best so far.
	if lt? [incpt .frac] [sli .bestfrac]
		[sli .bestfrac] = [incpt .frac]
		[sli .bestline] = ld
	endif

	; no need to visit any more lines
	return FALSE
end

;
; P_SlideMove
;
; The momx / momy move is bad, so try to slide along a wall.
; Find the first line hit, move flush to it, and slide along it.
; This is a kludgy mess.
;
fun P_SlideMove (mo ^mobj_t)
	[sli .mo] = mo

	hitcount s32 = 0

	loop
		; don't loop forever
		hitcount = iadd hitcount 1
		break if ge? hitcount 3

		; trace along the three leading corners
		r    = [mo .radius]
		momx = [mo .momx]
		momy = [mo .momy]

		if pos? momx
			leadx  = iadd [mo .x] r
			trailx = isub [mo .x] r
		else
			leadx  = isub [mo .x] r
			trailx = iadd [mo .x] r
		endif

		if pos? momy
			leady  = iadd [mo .y] r
			traily = isub [mo .y] r
		else
			leady  = isub [mo .y] r
			traily = iadd [mo .y] r
		endif

		[sli .bestfrac] = (FRACUNIT + 1)

		check_func = fun PTR_SlideTraverse

		P_PathTraverse leadx  leady  (iadd leadx  momx) (iadd leady  momy) PT_ADDLINES check_func
		P_PathTraverse trailx leady  (iadd trailx momx) (iadd leady  momy) PT_ADDLINES check_func
		P_PathTraverse leadx  traily (iadd leadx  momx) (iadd traily momy) PT_ADDLINES check_func

		; move up to the wall.
		; if move hit the middle, stairstep
		hit_nothing = eq? [sli .bestfrac] (FRACUNIT + 1)
		break if hit_nothing

		; fudge a bit to make sure it doesn't hit
		[sli .bestfrac] = isub [sli .bestfrac] 0x800

		if pos? [sli .bestfrac]
			newx = FixedMul momx [sli .bestfrac]
			newy = FixedMul momy [sli .bestfrac]

			newx = iadd [mo .x] newx
			newy = iadd [mo .y] newy

			ok = P_TryMove mo newx newy
			break if not ok
		endif

		; Now continue along the wall.
		; First calculate remainder.
		[sli .bestfrac] = iadd [sli .bestfrac] 0x800
		[sli .bestfrac] = isub FRACUNIT [sli .bestfrac]
		[sli .bestfrac] = imin [sli .bestfrac] FRACUNIT

		if le? [sli .bestfrac] 0
			return
		endif

		[sli .xmove] = FixedMul [mo .momx] [sli .bestfrac]
		[sli .ymove] = FixedMul [mo .momy] [sli .bestfrac]

		; clip the move
		P_HitSlideLine [sli .bestline]

		[mo .momx] = [sli .xmove]
		[mo .momy] = [sli .ymove]

		newx = iadd [mo .x] [mo .momx]
		newy = iadd [mo .y] [mo .momy]

		ok = P_TryMove mo newx newy
		if ok
			return
		endif
	endloop

	; stairstep

	newx = iadd [mo .x] [mo .momx]
	newy = iadd [mo .y] [mo .momy]

	ok = P_TryMove mo [mo .x] newy
	if not ok
		P_TryMove mo newx [mo .y]
	endif
end


;;
;; LINE ATTACK
;;

type LineAttackInfo struct
	.shooter  ^mobj_t   ; who attacked
	.target   ^mobj_t   ; who got hit (or NULL)

	.range     fixed_t  ; distance
	.shootz    fixed_t  ; height if not aiming up or down
	.damage    s32      ; amount of damage to inflict

	.aimslope    fixed_t
	.topslope    fixed_t  ; slopes to top and bottom of target
	.bottomslope fixed_t  ;
end

zero-var la LineAttackInfo

;
; PTR_AimTraverse
; Sets linetaget and aimslope when a target is aimed at.
;
fun PTR_AimTraverse (incpt ^intercept_t -> bool)
	if some? [incpt .is_line]
		return PTR_AimTraverseLine incpt
	endif

	; hit a thing

	mo   = [incpt .obj .thing]
	frac = [incpt .frac]

	if eq? mo [la .shooter]
		; can't shoot self
		return TRUE
	endif

	if zero? (iand [mo .flags] MF_SHOOTABLE)
		; corpse or something
		return TRUE
	endif

	; check angles to see if the thing can be aimed at
	topz = iadd [mo .z] [mo .height]
	dist = FixedMul [la .range] frac

	t_slope = FixedDiv (isub topz [la .shootz]) dist
	b_slope = FixedDiv (isub [mo .z] [la .shootz]) dist

	if lt? t_slope [la .bottomslope]
		; shot over the thing
		return TRUE
	endif

	if gt? b_slope [la .topslope]
		; shot under the thing
		return TRUE
	endif

	; this thing can be hit!
	t_slope = imin t_slope [la .topslope]
	b_slope = imax b_slope [la .bottomslope]

	[la .aimslope] = idivt (iadd t_slope b_slope) 2
	[la .target]   = mo

	; hit something, so don't check any farther
	return FALSE
end

;
; PTR_ShootTraverse
;
fun PTR_ShootTraverse (incpt ^intercept_t -> bool)
	if some? [incpt .is_line]
		return PTR_ShootTraverseLine incpt
	endif

	; shoot a thing

	mo   = [incpt .obj .thing]
	frac = [incpt .frac]

	if eq? mo [la .shooter]
		; can't shoot self
		return TRUE
	endif

	if zero? (iand [mo .flags] MF_SHOOTABLE)
		; corpse or something
		return TRUE
	endif

	; check angles to see if the thing can be aimed at
	topz = iadd [mo .z] [mo .height]
	dist = FixedMul [la .range] frac

	t_slope = FixedDiv (isub topz    [la .shootz]) dist
	b_slope = FixedDiv (isub [mo .z] [la .shootz]) dist

	if lt? t_slope [la .aimslope]
		; shot over the thing
		return TRUE
	endif

	if gt? b_slope [la .aimslope]
		; shot under the thing
		return TRUE
	endif

	; position a bit closer (away from the thing)
	step = FixedDiv (10 * FRACUNIT) [la .range]
	frac = isub frac step
	dist = FixedMul [la .range] frac

	x = FixedMul frac [trace .dx]
	y = FixedMul frac [trace .dy]
	z = FixedMul dist [la .aimslope]

	x = iadd x [trace .x]
	y = iadd y [trace .y]
	z = iadd z [la .shootz]

	; spawn bullet puffs or blood spots, depending on target type.
	if some? (iand [mo .flags] MF_NOBLOOD)
		P_SpawnPuff  x y z
	else
		P_SpawnBlood x y z [la .damage]
	endif

	if some? [la .damage]
		P_DamageMobj mo [la .shooter] [la .shooter] [la .damage]
	endif

	; hit something, so don't check any farther
	return FALSE
end

fun PTR_AimTraverseLine (incpt ^intercept_t -> bool)
	ld   = [incpt .obj .line]
	frac = [incpt .frac]

	if zero? (iand [ld .flags] ML_TWOSIDED)
		; hit a wall, so don't check any farther
		return FALSE
	endif

	; Crosses a two sided line.
	; A two sided line will restrict the possible target ranges.
	P_LineOpening ld

	if ge? [openbottom] [opentop]
		; hit a closed door, don't check any farther
		return FALSE
	endif

	dist    = FixedMul [la .range] frac
	b_slope = FixedDiv (isub [openbottom] [la .shootz]) dist
	t_slope = FixedDiv (isub [opentop]    [la .shootz]) dist

	front = [ld .front]
	back  = [ld .back]

	jump do_floor if null? back

	if ne? [front .floorh] [back .floorh]
		do_floor:
		[la .bottomslope] = imax [la .bottomslope] b_slope
	endif

	jump do_ceiling if null? back

	if ne? [front .ceilh] [back .ceilh]
		do_ceiling:
		[la .topslope] = imin [la .topslope] t_slope
	endif

	; stop if the gap has closed
	has_gap = gt? [la .topslope] [la .bottomslope]
	return has_gap
end

fun PTR_ShootTraverseLine (incpt ^intercept_t -> bool)
	ld   = [incpt .obj .line]
	frac = [incpt .frac]

	if some? [ld .special]
		P_ShootSpecialLine ld [la .shooter]
	endif

	front = [ld .front]
	back  = [ld .back]

	; crosses a two sided line?
	one_sided = zero? (iand [ld .flags] ML_TWOSIDED)
	jump hitline if one_sided

	P_LineOpening ld

	dist    = FixedMul [la .range] frac
	b_slope = FixedDiv (isub [openbottom] [la .shootz]) dist
	t_slope = FixedDiv (isub [opentop]    [la .shootz]) dist

	if null? back
		; e6y: emulation of missed back side on two-sided lines.
		; backsector can be NULL when emulating missing back side.

		jump hitline if gt? b_slope [la .aimslope]
		jump hitline if lt? t_slope [la .aimslope]
	else
		if ne? [front .floorh] [back .floorh]
			jump hitline if gt? b_slope [la .aimslope]
		endif

		if ne? [front .ceilh] [back .ceilh]
			jump hitline if lt? t_slope [la .aimslope]
		endif
	endif

	; shot passes the line
	return TRUE

hitline:
	; position a bit closer (away from the wall)
	step = FixedDiv (4 * FRACUNIT) [la .range]
	frac = isub frac step
	dist = FixedMul [la .range] frac

	x = FixedMul frac [trace .dx]
	y = FixedMul frac [trace .dy]
	z = FixedMul dist [la .aimslope]

	x = iadd x [trace .x]
	y = iadd y [trace .y]
	z = iadd z [la .shootz]

	; don't shoot the sky!
	if eq? [front .ceilpic] [skyflatnum]
		if gt? z [front .ceilh]
			return FALSE
		endif

		if ref? back
			if eq? [back .ceilpic] [skyflatnum]
				return FALSE
			endif
		endif
	endif

	P_SpawnPuff x y z

	; hit a wall, so don't check any farther
	return FALSE
end

;
; P_AimLineAttack
;
fun P_AimLineAttack (mo ^mobj_t angle angle_t dist fixed_t -> fixed_t)
	mo = P_SubstNullMobj mo

	[la .shooter] = mo
	[la .target]  = NULL
	[la .range]   = dist
	[la .shootz]  = iadd [mo .z] (ishr [mo .height] 1) (8 * FRACUNIT)

	; can't shoot outside view angles
	[la .topslope]    = SCREENSLOPE
	[la .bottomslope] = (- SCREENSLOPE)

	angle = ishr angle ANGLEFINESHIFT
	dist  = ishr dist  FRACBITS

	x2 = imul [finecosine angle] dist
	y2 = imul [finesine   angle] dist

	x2 = iadd x2 [mo .x]
	y2 = iadd y2 [mo .y]

	aim_func = fun PTR_AimTraverse

	P_PathTraverse [mo .x] [mo .y] x2 y2 (PT_ADDLINES | PT_ADDTHINGS) aim_func

	if ref? [la .target]
		return [la .aimslope]
	endif

	return 0
end

;
; P_LineAttack
;
fun P_LineAttack (mo ^mobj_t angle angle_t dist fixed_t slope fixed_t damage s32)
	[la .shooter]  = mo
	[la .range]    = dist
	[la .aimslope] = slope
	[la .damage]   = damage
	[la .shootz]   = iadd [mo .z] (ishr [mo .height] 1) (8 * FRACUNIT)

	angle = ishr angle ANGLEFINESHIFT
	dist  = ishr dist  FRACBITS

	x2 = imul [finecosine angle] dist
	y2 = imul [finesine   angle] dist

	x2 = iadd x2 [mo .x]
	y2 = iadd y2 [mo .y]

	shoot_func = fun PTR_ShootTraverse

	P_PathTraverse [mo .x] [mo .y] x2 y2 (PT_ADDLINES | PT_ADDTHINGS) shoot_func
end


;;
;; USE LINES
;;

zero-var usething ^mobj_t

fun PTR_UseTraverse (incpt ^intercept_t -> bool)
	ld = [incpt .obj .line]
	mo = [usething]

	if zero? [ld .special]
		P_LineOpening ld

		if le? [openrange] 0
			; can't use through a wall
			S_StartSound (raw-cast mo) sfx_noway
			return FALSE
		endif

		; not a special line, but keep checking
		return TRUE
	endif

	side = P_PointOnLineSide [mo .x] [mo .y] ld

	P_UseSpecialLine ld side mo

	; can't use more than one special line in a row
	return FALSE
end

;
; P_UseLines
; Looks for special lines in front of the player to activate.
;
fun P_UseLines (p ^player_t)
	mo = [p .mo]

	[usething] = mo

	ang = ishr [mo .angle] ANGLEFINESHIFT
	dx  = imul [finecosine ang] (USERANGE >> FRACBITS)
	dy  = imul [finesine   ang] (USERANGE >> FRACBITS)

	x1 = [mo .x]
	y1 = [mo .y]
	x2 = iadd x1 dx
	y2 = iadd y1 dy

	check_func = fun PTR_UseTraverse

	P_PathTraverse x1 y1 x2 y2 PT_ADDLINES check_func
end


;;
;; RADIUS ATTACK
;;

type BombInfo struct
	.spot    ^mobj_t
	.source  ^mobj_t
	.damage   s32
end

; this is da bomb
zero-var bomb BombInfo

;
; PIT_RadiusAttack
; "bomb.source" is the creature that caused the explosion at "bomb.spot".
;
fun PIT_RadiusAttack (mo ^mobj_t -> bool)
	if zero? (iand [mo .flags] MF_SHOOTABLE)
		return TRUE
	endif

	; Boss spider and cyborg take no damage from concussion.
	spider = eq? [mo .type] MT_SPIDER
	cyborg = eq? [mo .type] MT_CYBORG
	if or spider cyborg
		return TRUE
	endif

	spot   = [bomb .spot]
	damage = [bomb .damage]

	dx = isub [mo .x] [spot .x]
	dy = isub [mo .y] [spot .y]

	dist s32 = imax (iabs dx) (iabs dy)

	dist = isub dist [mo .radius]
	dist = ishr dist FRACBITS
	dist = imax dist 0

	; in range? and a clear line of sight?
	if gt? damage dist
		damage = isub damage dist

		los = P_CheckSight mo spot
		if los
			P_DamageMobj mo spot [bomb .source] damage
		endif
	endif

	return TRUE
end

;
; P_RadiusAttack
; Source is the creature that caused the explosion at spot.
;
fun P_RadiusAttack (spot ^mobj_t source ^mobj_t damage s32)
	[bomb .spot]   = spot
	[bomb .source] = source
	[bomb .damage] = damage

	dist fixed_t = iadd damage MAXRADIUS
	dist = ishl dist FRACBITS

	bx1 = Block_X (isub [spot .x] dist)
	by1 = Block_Y (isub [spot .y] dist)
	bx2 = Block_X (iadd [spot .x] dist)
	by2 = Block_Y (iadd [spot .y] dist)

	attack_func = fun PIT_RadiusAttack

	by = by1
	loop while le? by by2
		bx = bx1
		loop while le? bx bx2
			P_BlockThingsIterator bx by attack_func
			bx = iadd bx 1
		endloop
		by = iadd by 1
	endloop
end

;;
;; SECTOR HEIGHT CHANGING
;;

; After modifying a sectors floor or ceiling height,
; call this routine to adjust the positions
; of all things that touch the sector.
;
; If anything doesn't fit anymore, true will be returned.
; If crunch is true, they will take damage as they are being crushed.
; If Crunch is false, you should set the sector height back
; the way it was and call P_ChangeSector again to undo the changes.
;
type ChangeSectorInfo struct
	.crush  bool
	.nofit  bool
end

zero-var changeinfo ChangeSectorInfo

;
; PIT_ChangeSector
;
fun PIT_ChangeSector (mo ^mobj_t -> bool)
	ok = P_ThingHeightClip mo
	if ok
		return TRUE
	endif

	; crunch bodies to giblets
	if le? [mo .health] 0
		P_SetMobjState mo S_GIBS

		[mo .flags]  = iand [mo .flags] (~ MF_SOLID)
		[mo .height] = 0
		[mo .radius] = 0

		return TRUE
	endif

	; crunch dropped items
	if some? (iand [mo .flags] MF_DROPPED)
		P_RemoveMobj mo
		return TRUE
	endif

	; non-solid? assume it is bloody gibs or something
	if zero? (iand [mo .flags] MF_SHOOTABLE)
		return TRUE
	endif

	[changeinfo .nofit] = TRUE

	if not [changeinfo .crush]
		return TRUE
	endif

	t = iand [leveltime] 3

	if zero? t
		P_DamageMobj mo NULL NULL 10

		; spray blood in a random direction
		z = iadd [mo .z] (idivt [mo .height] 2)

		blood = P_SpawnMobj [mo .x] [mo .y] z MT_BLOOD

		rx = P_SubRandom
		ry = P_SubRandom

		[blood .momx] = ishl rx 12
		[blood .momy] = ishl ry 12
	endif

	; keep checking (crush other things)
	return TRUE
end

;
; P_ChangeSector
;
fun P_ChangeSector (sec ^sector_t crush bool -> bool)

	[changeinfo .crush] = crush
	[changeinfo .nofit] = FALSE

	bx1 = [sec .blockbox BOXLEFT]
	by1 = [sec .blockbox BOXBOTTOM]
	bx2 = [sec .blockbox BOXRIGHT]
	by2 = [sec .blockbox BOXTOP]

	change_func = fun PIT_ChangeSector

	bx = bx1
	loop while le? bx bx2
		by = by1
		loop while le? by by2
			P_BlockThingsIterator bx by change_func
			by = iadd by 1
		endloop
		bx = iadd bx 1
	endloop

	return [changeinfo .nofit]
end
