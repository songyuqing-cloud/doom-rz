;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const DOORSPEED = (FRACUNIT * 2)
const DOORWAIT  = 150

type door_type_e s32

const dr_normal       = 0
const dr_close30Open  = 1
const dr_close        = 2
const dr_open         = 3
const dr_raiseIn5Min  = 4
const dr_blazeRaise   = 5
const dr_blazeOpen    = 6
const dr_blazeClose   = 7

type door_t struct
	.thinker     thinker_t
	.sector     ^sector_t

	.type        door_type_e
	.speed       fixed_t
	.top         fixed_t

	.direction   s32
	.wait        s32   ; tics to wait at the top
	.count       s32   ; countdown when waiting
end

const DIR_WAIT = 0

; strings...
const PD_BLUE_OBJ   = "You need a blue key to activate this object"
const PD_RED_OBJ    = "You need a red key to activate this object"
const PD_YELLOW_OBJ = "You need a yellow key to activate this object"

const PD_BLUE_KEY   = "You need a blue key to open this door"
const PD_RED_KEY    = "You need a red key to open this door"
const PD_YELLOW_KEY = "You need a yellow key to open this door"

;
; T_VerticalDoor
;
fun T_VerticalDoor (D ^door_t)
	sec  = [D .sector]
	kind = [D .type]

	if eq? [D .direction] DIR_WAIT
		; WAITING
		[D .count] = isub [D .count] 1

		if zero? [D .count]
			if eq? kind dr_blazeRaise
				; time to go back down
				[D .direction] = -1
				S_StartSound [addr-of sec .soundorg] sfx_bdcls

			elif eq? kind dr_normal
				; time to go back down
				[D .direction] = -1
				S_StartSound [addr-of sec .soundorg] sfx_dorcls

			elif eq? kind dr_close30Open
				[D .direction] = +1
				S_StartSound [addr-of sec .soundorg] sfx_doropn

			elif eq? kind dr_raiseIn5Min
				[D .type] = dr_normal
				[D .direction] = +1

				S_StartSound [addr-of sec .soundorg] sfx_doropn
			endif
		endif

		return
	endif

	if neg? [D .direction]
		; DOWN
		res = T_MoveCeilingPlane [D .sector] [D .speed] [sec .floorh] FALSE -1

		if eq? res RES_pastdest
			if eq? kind dr_blazeRaise dr_blazeClose
				; unlink and free
				P_RemoveThinker (raw-cast D)
				[sec .specialdata] = NULL

				S_StartSound [addr-of sec .soundorg] sfx_bdcls

			elif eq? kind dr_normal dr_close
				; unlink and free
				P_RemoveThinker (raw-cast D)
				[sec .specialdata] = NULL

			elif eq? kind dr_close30Open
				[D .direction] = DIR_WAIT
				[D .count]     = (TICRATE * 30)
			endif

		elif eq? res RES_crushed
			if eq? kind dr_close dr_blazeClose
				; DO NOT GO BACK UP!
			else
				[D .direction] = +1
				S_StartSound [addr-of sec .soundorg] sfx_doropn
			endif
		endif

		return
	endif

	if pos? [D .direction]
		; UP
		res = T_MoveCeilingPlane [D .sector] [D .speed] [D .top] FALSE +1

		if eq? res RES_pastdest
			if eq? kind dr_normal dr_blazeRaise
				; wait at top
				[D .direction] = DIR_WAIT
				[D .count]     = [D .wait]

			elif eq? kind dr_open dr_blazeOpen dr_close30Open
				; unlink and free
				P_RemoveThinker (raw-cast D)
				[sec .specialdata] = NULL
			endif
		endif

		return
	endif
end

;
; EV_DoLockedDoor
; Move a locked door up/down
;
fun EV_DoLockedDoor (ld ^line_t kind door_type_e mo ^mobj_t -> bool)
	p = [mo .player]

	if null? p
		return FALSE
	endif

	spec = [ld .special]

	if eq? spec 99 133
		; Blue Lock
		has_key = or [p .cards it_bluecard] [p .cards it_blueskull]

		if not has_key
			[p .message] = PD_BLUE_OBJ

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	if eq? spec 134 135
		; Red Lock
		has_key = or [p .cards it_redcard] [p .cards it_redskull]

		if not has_key
			[p .message] = PD_RED_OBJ

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	if eq? spec 136 137
		; Yellow Lock
		has_key = or [p .cards it_yellowcard] [p .cards it_yellowskull]

		if not has_key
			[p .message] = PD_YELLOW_OBJ

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	return EV_DoDoor ld kind
end

;
;  Normal (tagged) door
;
fun EV_DoDoor (ld ^line_t kind door_type_e -> bool)
	did = FALSE

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		if null? [sec .specialdata]
			; new door thinker
			D ^door_t = Z_Calloc door_t.size PU_LEVEL

			[D .thinker .func] = THINK_door

			; set default parameters, adjusted below
			[D .sector]    = sec
			[D .type]      = kind
			[D .speed]     = DOORSPEED
			[D .wait]      = DOORWAIT
			[D .direction] = +1
			[D .top]       = P_FindLowestCeilingSurrounding sec
			[D .top]       = isub [D .top] (4 * FRACUNIT)

			if eq? kind dr_blazeClose
				[D .speed]     = (DOORSPEED * 4)
				[D .direction] = -1
				S_StartSound [addr-of sec .soundorg] sfx_bdcls

			elif eq? kind dr_close
				[D .direction] = -1
				S_StartSound [addr-of sec .soundorg] sfx_dorcls

			elif eq? kind dr_close30Open
				[D .top] = [sec .ceilh]
				[D .direction] = -1
				S_StartSound [addr-of sec .soundorg] sfx_dorcls

			elif eq? kind dr_blazeRaise dr_blazeOpen
				[D .speed] = (DOORSPEED * 4)
				if ne? [D .top] [sec .ceilh]
					S_StartSound [addr-of sec .soundorg] sfx_bdopn
				endif

			elif eq? kind dr_normal dr_open
				if ne? [D .top] [sec .ceilh]
					S_StartSound [addr-of sec .soundorg] sfx_doropn
				endif
			endif

			P_AddThinker (raw-cast D)
			[sec .specialdata] = D

			did = TRUE
		endif
	endloop

	return did
end

;
; EV_ManualDoor : open a door manually, no tag value
;
fun EV_ManualDoor (ld ^line_t mo ^mobj_t -> bool)
	p    = [mo .player]
	spec = [ld .special]

	; check for locks...

	if eq? spec 26 32
		; Blue Lock

		if null? p
			return FALSE
		endif

		has_key = or [p .cards it_bluecard] [p .cards it_blueskull]

		if not has_key
			[p .message] = PD_BLUE_KEY

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	if eq? spec 28 33
		; Red Lock

		if null? p
			return FALSE
		endif

		has_key = or [p .cards it_redcard] [p .cards it_redskull]

		if not has_key
			[p .message] = PD_RED_KEY

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	if eq? spec 27 34
		; Yellow Lock

		if null? p
			return FALSE
		endif

		has_key = or [p .cards it_yellowcard] [p .cards it_yellowskull]

		if not has_key
			[p .message] = PD_YELLOW_KEY

			S_StartHudSound (raw-cast [p .mo]) sfx_oof
			return FALSE
		endif
	endif

	; only front sides can be used

	if neg? [ld .sidenum 1]
		; andrewj: ignore this instead of bombing out
		; I_Error("EV_ManualDoor: DR special type on 1-sided linedef")
		return FALSE
	endif

	; if the sector has an active thinker, use it
	sec = [ld .back]

	th ^thinker_t = [sec .specialdata]
	if ref? th
		; ONLY FOR "RAISE" DOORS, NOT "OPEN"s
		if eq? spec 1 26 27 28 117
			is_plat = eq? [th .func] THINK_plat
			is_door = eq? [th .func] THINK_door

			; When is a door not a door?
			; In Vanilla, door->direction is set, even though
			; "specialdata" might not actually point at a door.

			if is_plat
				P ^plat_t = [sec .specialdata]

				if null? p
					return FALSE
				endif

				; The direction field in door_t corresponds to the wait
				; field in plat_t.  Let's set that to -1 instead.

				[P .wait] = -1

			elif is_door
				D ^door_t = [sec .specialdata]

				if neg? [D .direction]
					; go back up
					[D .direction] = +1
				elif null? p
					; JDC: bad guys never close doors
					return FALSE
				else
					; start going down immediately
					[D .direction] = -1
				endif

			else
				; This isn't a door OR a plat.  Now we're in trouble.
				I_Print "EV_ManualDoor: Tried to close a non-door.\n"
			endif

			return TRUE
		endif
	endif

	; for proper sound
	sfx sfxenum_t = sfx_doropn

	if eq? spec 117 118
		; BLAZING DOOR
		sfx = sfx_bdopn
	endif

	S_StartSound [addr-of sec .soundorg] sfx

	; new door thinker
	D ^door_t = Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	; set default parameters, adjusted below
	[D .sector]    = sec
	[D .type]      = dr_normal
	[D .speed]     = DOORSPEED
	[D .wait]      = DOORWAIT
	[D .direction] = +1

	if and (ge? spec 31) (le? spec 34)
		[D .type] = dr_open
		[ld .special] = 0

	elif eq? spec 117
		; blazing door raise
		[D .type]  = dr_blazeRaise
		[D .speed] = (DOORSPEED * 4)

	elif eq? spec 118
		; blazing door open
		[D .type]  = dr_blazeOpen
		[D .speed] = (DOORSPEED * 4)
		[ld .special] = 0
	endif

	; find the top of the movement range
	[D .top] = P_FindLowestCeilingSurrounding sec
	[D .top] = isub [D .top] (4 * FRACUNIT)

	P_AddThinker (raw-cast D)
	[sec .specialdata] = D

	return TRUE
end

;
; Spawn a door that closes after 30 seconds
;
fun P_SpawnDoorCloseIn30 (sec ^sector_t)
	D ^door_t = Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	[D .sector]    = sec
	[D .type]      = dr_normal
	[D .speed]     = DOORSPEED
	[D .top]       = 0
	[D .direction] = DIR_WAIT
	[D .wait]      = DOORWAIT
	[D .count]     = (30 * TICRATE)

	P_AddThinker (raw-cast D)

	[sec .specialdata] = D
	[sec .special] = 0
end

;
; Spawn a door that opens after 5 minutes
;
fun P_SpawnDoorRaiseIn5Mins (sec ^sector_t)
	D ^door_t = Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	[D .sector]    = sec
	[D .type]      = dr_raiseIn5Min
	[D .speed]     = DOORSPEED
	[D .top]       = P_FindLowestCeilingSurrounding sec
	[D .top]       = isub [D .top] (4 * FRACUNIT)
	[D .direction] = DIR_WAIT
	[D .wait]      = DOORWAIT
	[D .count]     = (300 * TICRATE)

	P_AddThinker (raw-cast D)

	[sec .specialdata] = D
	[sec .special] = 0
end
