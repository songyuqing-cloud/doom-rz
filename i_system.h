//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//	System specific interface stuff.
//


#ifndef __I_SYSTEM__
#define __I_SYSTEM__

#include <stdio.h>

#include "d_ticcmd.h"
#include "d_event.h"


// Endianness
signed short SHORT(signed short v);
signed int   LONG (signed int   v);

signed short BE_SHORT(signed short v);
signed int   BE_LONG (signed int   v);


// Asynchronous interrupt functions should maintain private queues
// that are read by the synchronous functions
// to be converted into events.

// Either returns a null ticcmd,
// or calls a loadable driver to build it.
// This ticcmd will then be modified by the gameloop
// for normal input.
ticcmd_t* I_BaseTiccmd (void);


// Called by M_Responder when quit is selected.
// Clean exit, displays sell blurb.
void I_Quit (void);

void I_Print  (const char *msg);
void I_Print2 (const char *msg, const char *arg);
void I_Print3 (const char *msg, int arg);

void I_Error (const char *error);
void I_Error2(const char *error, const char *arg);

void I_Tactile (int on, int off, int total);

void *I_Alloc(unsigned int size);
void *I_Realloc(void *ptr, unsigned int size);
void  I_Free(void *ptr);

void I_MemSet(void *ptr,  int c, unsigned int size);
void I_MemCopy(void *dest, const void *src, unsigned int size);

// Print startup banner copyright message.

void I_PrintStartupBanner(char *gamedescription);

// Print a centered text banner displaying the given string.

void I_PrintBanner(char *text);

// Print a dividing line for startup banners.

void I_PrintDivider(void);


// some file I/O wrappers

FILE *  I_Fopen (const char *path, const char *mode);
int     I_Fclose(FILE *f);
boolean I_Feof  (FILE *f);
void    I_Fflush(FILE *f);

int I_Fread (void *buf, int size, FILE *f);
int I_Fwrite(const void *buf, int size, FILE *f);
int I_Fputc (int c, FILE *f);
int I_Fseek (FILE *f, int offset, int whence);
int I_Ftell (FILE *f);

char I_FileOrDirExists(const char *filename);
boolean I_MakeDirectory(const char *dir);
boolean I_DeleteFile(const char *filename);
boolean I_RenameFile(const char *oldname, const char *newname);

const char *I_Getenv(const char *name);

#endif
