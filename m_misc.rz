;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

;;
;; Determine the length of an open file.
;;
fun M_FileLength (handle ^FILE -> s32)
	; save the current position in the file
	savedpos = I_Ftell handle

	; jump to the end to find the length
	I_Fseek handle 0 SEEK_END

	length = I_Ftell handle

	; go back to the old location
	I_Fseek handle savedpos SEEK_SET

	return length
end

fun M_WriteFile (name ^uchar buffer ^raw-mem length s32 -> bool)
	handle = I_Fopen name "wb"

	if null? handle
		return FALSE
	endif

	count = I_Fwrite buffer length handle
	I_Fclose handle

	return ge? count length
end

;; Check if a file exists by probing for common case variation of its filename.
;; Returns a newly allocated string that the caller is responsible for freeing.

fun M_FileCaseExists (path ^uchar -> ^uchar)
	path = M_StringDuplicate path

	; transform directory separators to Unixy ones
	s = path
	loop
		ch = [s]
		break if zero? ch
		if eq? ch '\\'
			[s] = '/'
		endif
		s = padd s 1
	endloop

	; 0 - the given path
	exist = I_FileOrDirExists path
	if not (zero? exist)
		return path
	endif

	basename = padd path (M_StrLen path)
	basename = psub basename 1

	loop
		break if le? basename path
		break if eq? [basename] '/'

		basename = psub basename 1
	endloop

	if eq? [basename] '/'
		basename = padd basename 1
	endif

	; 1 - lowercase filename, e.g. doom2.wad
	M_ForceLowercase basename

	exist = I_FileOrDirExists path
	if not (zero? exist)
		return path
	endif

	; 2 - uppercase filename, e.g. DOOM2.WAD
	M_ForceUppercase basename

	exist = I_FileOrDirExists path
	if not (zero? exist)
		return path
	endif

	; 3 - uppercase basename with lowercase extension, e.g. DOOM2.wad
	ext = padd basename (M_StrLen basename)
	ext = psub ext 1

	loop
		break if le? ext basename
		break if eq? [ext] '.'

		ext = psub ext 1
	endloop

	if gt? ext basename
		M_ForceLowercase ext

		exist = I_FileOrDirExists path
		if not (zero? exist)
			return path
		endif
	endif

	; 4 - lowercase filename with uppercase first letter, e.g. Doom2.wad
	if gt? (M_StrLen basename) 1
		M_ForceLowercase (padd basename 1)

		exist = I_FileOrDirExists path
		if not (zero? exist)
			return path
		endif
	endif

	; 5 - no luck
	I_Free path
	return NULL
end

fun M_ExtractFileBase(path ^uchar dest ^[8]uchar)
	I_MemSet dest 0 8

	src = padd path (M_StrLen path)
	src = psub src  1

	; back up until a backslash or the start
	loop
		if le? src path
			jump truncate
		endif

		ch = [src]

		break if eq? ch '/'
		break if eq? ch '\\'

		src = psub src 1
	endloop

	src = padd src 1

truncate:
	filename = src

	; Copy up to eight characters
	; Note: Vanilla Doom exits with an error if a filename is specified
	; with a base of more than eight characters.  To remove the 8.3
	; filename limit, instead we simply truncate the name.

	length u32 = 0

	loop
		ch = [src]

		break if zero? ch
		break if eq? ch '.'

		if ge? length 8
			I_Print2 "Warning: Truncated '%s' lump name\n" filename
			break
		endif

		[dest length] = M_ToUpper ch
		length = iadd length 1
	endloop
end

fun M_Atoi (str ^uchar -> s32)
	result s32 = 0

	; skip whitespace
	loop while eq? [str] ' '
		str = padd str 1
	endloop

	is_neg = eq? [str] '-'
	if is_neg
		str = padd str 1
	endif

	loop
		ch = [str]
		is_digit = and (ge? ch '0') (le? ch '9')

		break unless is_digit

		digit  = conv s32 (isub ch '0')
		result = imul result 10
		result = iadd result digit

		str = padd str 1
	endloop

	if is_neg
		result = ineg result
	endif

	return result
end

fun M_Itoa (buf ^uchar value s32)
	if neg? value
		[buf] = '-'
		buf   = padd buf 1
		value = ineg value
	endif

	t s32 = 1000000000
	seen_digit bool = FALSE

	loop until zero? t
		if or (ge? value t) seen_digit
			digit = idivt value t
			value = isub  value (imul digit t)

			if pos? digit
				seen_digit = TRUE
			endif

			[buf] = iadd '0' (conv uchar digit)
			buf   = padd buf 1
		endif

		t = idivt t 10
	endloop

	if not seen_digit
		[buf] = '0'
		buf   = padd buf 1
	endif

	[buf] = 0
end

fun M_ForceUppercase (p ^uchar)
	loop
		ch = [p]
		break if zero? ch
		[p] = M_ToUpper ch
		p   = padd p 1
	endloop
end

fun M_ForceLowercase (p ^uchar)
	loop
		ch = [p]
		break if zero? ch
		[p] = M_ToLower ch
		p   = padd p 1
	endloop
end

;;
;; Safe version of strdup() that checks the string was successfully
;; allocated.
;;
fun M_StringDuplicate (orig ^uchar -> ^uchar)
	len = M_StrLen orig
	len = iadd len 1

	result ^uchar = I_Alloc len
	I_MemCopy result orig len

	return result
end

;; Return a newly-malloced string with all the strings given as arguments
;; concatenated together.

fun M_StringJoin (A ^uchar B ^uchar -> ^uchar)
	return M_StringJoin3 A B ""
end

fun M_StringJoin3 (A ^uchar B ^uchar C ^uchar -> ^uchar)
	A_len = M_StrLen A
	B_len = M_StrLen B
	C_len = M_StrLen C

	new_len = iadd A_len   B_len
	new_len = iadd new_len C_len
	new_len = iadd new_len 1

	result ^uchar = I_Alloc new_len

	I_MemCopy result A A_len

	B_store = padd result A_len

	I_MemCopy B_store B B_len

	C_store = padd B_store B_len
	C_len   = iadd C_len 1

	I_MemCopy C_store C C_len
	return result
end

;; Safe string copy function that works like OpenBSD's strlcpy().
;; Returns true if the string was not truncated.

fun M_StringCopy (dest ^uchar src ^uchar destsize u32 -> bool)
	len = M_StrLen src
	len = iadd len 1

	if lt? len destsize
		I_MemCopy dest src len
		return TRUE
	endif

	if pos? destsize
		destsize = isub destsize 1
		I_MemCopy dest src destsize

		dest = padd dest destsize
		[dest] = 0
	endif

	return FALSE
end

;; Safe string concat function that works like OpenBSD's strlcat().
;; Returns true if string not truncated.

fun M_StringConcat (dest ^uchar src ^uchar destsize u32 -> bool)
	offset = M_StrLen dest

	if gt? offset destsize
		offset = destsize
	endif

	dest     = padd dest offset
	destsize = isub destsize offset

	return M_StringCopy dest src destsize
end

;; Returns true if 's' begins with the specified prefix.

fun M_StringStartsWith (s ^uchar prefix ^uchar -> bool)
	s_len = M_StrLen s
	p_len = M_StrLen prefix

	if le? s_len p_len
		return FALSE
	endif

	cmp = M_StrNCmp s prefix p_len
	return zero? cmp
end

;; Returns true if 's' ends with the specified suffix.

fun M_StringEndsWith (s ^uchar suffix ^uchar -> bool)
	s_len = M_StrLen s
	p_len = M_StrLen suffix

	if le? s_len p_len
		return FALSE
	endif

	s = padd s s_len
	s = psub s p_len

	cmp = M_StrNCmp s suffix p_len
	return zero? cmp
end

; andrewj: added this, quite simplistic
fun M_FormatNum (buf ^uchar buflen u32 s ^uchar arg s32)
	number = stack-var [64]uchar
	M_Itoa number arg

	loop
		ch = [s]

		break if zero? ch
		break if le? buflen 1

		next   = [(padd s 1)]
		is_fmt = and (eq? ch '%') (eq? next 'd')

		if is_fmt
			n ^uchar = number

			loop
				ch = [n]

				break if zero? ch
				break if le? buflen 1

				[buf]  = ch

				n      = padd n 1
				buf    = padd buf 1
				buflen = isub buflen 1
			endloop

			s = padd s 2
		else
			[buf]  = ch

			s      = padd s 1
			buf    = padd buf 1
			buflen = isub buflen 1
		endif
	endloop

	[buf] = 0
end

fun M_StrCmp (A ^uchar B ^uchar -> s32)
	loop
		AC = [A]
		BC = [B]

		if zero? (ior AC BC)
			return 0
		endif

		; this test also catches end-of-string conditions
		if ne? AC BC
			return isub (conv s32 AC) (conv s32 BC)
		endif

		A = padd A 1
		B = padd B 1
	endloop
end

fun M_StrNCmp (A ^uchar B ^uchar n u32 -> s32)
	loop
		AC = [A]
		BC = [B]

		if zero? n
			return 0
		endif

		if zero? (ior AC BC)
			return 0
		endif

		; this test also catches end-of-string conditions
		if ne? AC BC
			return isub (conv s32 AC) (conv s32 BC)
		endif

		A = padd A 1
		B = padd B 1
		n = isub n 1
	endloop
end

fun M_StrCaseCmp (A ^uchar B ^uchar -> s32)
	loop
		AC = M_ToUpper [A]
		BC = M_ToUpper [B]

		if zero? (ior AC BC)
			return 0
		endif

		; this test also catches end-of-string conditions
		if ne? AC BC
			return isub (conv s32 AC) (conv s32 BC)
		endif

		A = padd A 1
		B = padd B 1
	endloop
end

fun M_StrNCaseCmp (A ^uchar B ^uchar n u32 -> s32)
	loop
		AC = M_ToUpper [A]
		BC = M_ToUpper [B]

		if zero? n
			return 0
		endif

		if zero? (ior AC BC)
			return 0
		endif

		; this test also catches end-of-string conditions
		if ne? AC BC
			return isub (conv s32 AC) (conv s32 BC)
		endif

		A = padd A 1
		B = padd B 1
		n = isub n 1
	endloop
end

fun M_StrLen (s ^uchar -> u32)
	count u32 = 0

	loop until zero? [s]
		s     = padd s 1
		count = iadd count 1
	endloop

	return count
end

fun M_ToLower (ch uchar -> uchar)
	is_upper = and (ge? ch 'A') (le? ch 'Z')

	if is_upper
		ch = iadd ch 32
	endif

	return ch
end

fun M_ToUpper (ch uchar -> uchar)
	is_lower = and (ge? ch 'a') (le? ch 'z')

	if is_lower
		ch = isub ch 32
	endif

	return ch
end

fun M_IsSpace (ch uchar -> bool)
	; this is fairly lax
	if pos? ch
		if le? ch ' '
			return TRUE
		endif
	endif

	return FALSE
end

;
; Hash function used for lump names and texture names.
;
fun M_HashName8 (s ^uchar total u32 -> u32)
	; This is the djb2 string hash function, modded to work on strings
	; that have a maximum length of 8.

	if zero? total
		return 0
	endif

	result u32 = 5381

	i u32 = 0
	loop while lt? i 8
		ch = [s]
		break if zero? ch

		ch = M_ToUpper ch

		result = ixor result (ishl result 5)
		result = ixor result (conv u32 ch)

		i = iadd i 1
		s = padd s 1
	endloop

	result = iremt result total
	return result
end

fun M_HasExtension (filename ^uchar ext ^uchar -> bool)
	len = M_StrLen filename
	if zero? len
		return FALSE
	endif

	f = padd filename (M_StrLen filename)

	loop
		f = psub f 1

		if le? f filename
			return FALSE
		endif

		if eq? [f] '.'
			f = padd f 1
			cmp = M_StrCaseCmp f ext
			return zero? cmp
		endif
	endloop
end
