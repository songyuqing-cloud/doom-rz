#
#  Requires GNU make.
#

PROGRAM=doom-rz

# CC=gcc
# CC=clang-6.0
# CC=tcc -DSDL_DISABLE_IMMINTRIN_H

RAZM=./razm
NASM=nasm -f elf64

MISC=-std=c99
WARNINGS=-Wall -Wextra -Wno-implicit-fallthrough -Wno-sign-compare \
         -Wno-shadow -Wno-unused-parameter -Wno-unused-function
OPTIMISE=-O1

# default flags for compiler, preprocessor and linker
CFLAGS ?= $(MISC) $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

BUILD_DIR=_build

DUMMY=$(BUILD_DIR)/zzdummy

#----- Libraries ----------------------------------------------

CPPFLAGS += -I/usr/include/SDL2

LIBS += -lSDL2 -lSDL2_mixer

#----- Internal modules ------------------------------------------

MOD_COMMON=common.rz m_argv.rz m_bbox.rz m_misc.rz m_random.rz \
	m_zone.rz m_tables.rz m_math.rz m_memio.rz \
	system_api.rz

MOD_WAD=wad.rz w_main.rz w_file.rz w_wad.rz w_merge.rz \
	common.rz common_api.rz \
	system_api.rz

MOD_SOUND=sound.rz s_info.rz s_playback.rz \
	wad.rz    wad_api.rz \
	common.rz common_api.rz \
	system_api.rz

MOD_PLAY=play.rz play_def.rz p_info.rz \
	p_ceiling.rz p_doors.rz p_enemy.rz \
	p_floor.rz p_inter.rz p_lights.rz \
	p_map.rz p_maputl.rz p_mobj.rz p_plats.rz \
	p_savegame.rz p_setup.rz p_sight.rz \
	p_spec.rz p_switch.rz p_teleport.rz \
	p_tick.rz p_user.rz p_weapon.rz \
	sound.rz  sound_api.rz \
	wad.rz    wad_api.rz \
	common.rz common_api.rz \
	system_api.rz

MOD_RENDER=render.rz r_draw.rz \
	play.rz play_def.rz play_api.rz \
	sound.rz  sound_api.rz \
	wad.rz    wad_api.rz \
	common.rz common_api.rz \
	system_api.rz

MOD_UI=ui.rz v_automap.rz v_cheat.rz v_finale.rz \
	v_palette.rz v_status.rz v_texts.rz v_video.rz v_wipe.rz \
	render.rz render_api.rz \
	play.rz play_def.rz play_api.rz \
	sound.rz  sound_api.rz \
	wad.rz    wad_api.rz \
	common.rz common_api.rz \
	system_api.rz

MOD_ENGINE=engine.rz d_config.rz d_iwad.rz d_keys.rz \
	d_event.rz d_loop.rz d_main.rz d_game.rz \
	ui.rz     ui_api.rz \
	render.rz render_api.rz \
	play.rz   play_def.rz  play_api.rz \
	sound.rz  sound_api.rz \
	wad.rz    wad_api.rz \
	common.rz common_api.rz \
	system_api.rz

#----- Object files ----------------------------------------------

all: $(DUMMY) $(PROGRAM)

OBJS = \
	$(BUILD_DIR)/c/hu_lib.o   \
	$(BUILD_DIR)/c/hu_stuff.o   \
	$(BUILD_DIR)/c/i_input.o     \
	$(BUILD_DIR)/c/i_main.o      \
	$(BUILD_DIR)/c/i_oplmusic.o  \
	$(BUILD_DIR)/c/i_pcx.o   \
	$(BUILD_DIR)/c/i_sdlsound.o  \
	$(BUILD_DIR)/c/i_sound.o   \
	$(BUILD_DIR)/c/i_system.o  \
	$(BUILD_DIR)/c/i_timer.o   \
	$(BUILD_DIR)/c/i_video.o   \
	$(BUILD_DIR)/c/m_fixed.o  \
	$(BUILD_DIR)/c/m_menu.o   \
	$(BUILD_DIR)/c/midifile.o  \
	$(BUILD_DIR)/c/mus2mid.o   \
	$(BUILD_DIR)/c/opl3.o      \
	$(BUILD_DIR)/c/opl_queue.o  \
	$(BUILD_DIR)/c/opl_sdl.o   \
	$(BUILD_DIR)/c/r_bsp.o    \
	$(BUILD_DIR)/c/r_data.o   \
	$(BUILD_DIR)/c/r_draw.o   \
	$(BUILD_DIR)/c/r_main.o   \
	$(BUILD_DIR)/c/r_plane.o  \
	$(BUILD_DIR)/c/r_segs.o   \
	$(BUILD_DIR)/c/r_sky.o    \
	$(BUILD_DIR)/c/r_things.o   \
	$(BUILD_DIR)/c/wi_stuff.o \
	\
	$(BUILD_DIR)/rz/common.o  \
	$(BUILD_DIR)/rz/wad.o     \
	$(BUILD_DIR)/rz/ui.o      \
	$(BUILD_DIR)/rz/render.o  \
	$(BUILD_DIR)/rz/sound.o   \
	$(BUILD_DIR)/rz/play.o    \
	$(BUILD_DIR)/rz/engine.o

$(BUILD_DIR)/rz/common.s: $(MOD_COMMON)
	$(RAZM) $(MOD_COMMON) -o $@

$(BUILD_DIR)/rz/wad.s: $(MOD_WAD)
	$(RAZM) $(MOD_WAD) -o $@

$(BUILD_DIR)/rz/ui.s: $(MOD_UI)
	$(RAZM) $(MOD_UI) -o $@

$(BUILD_DIR)/rz/render.s: $(MOD_RENDER)
	$(RAZM) $(MOD_RENDER) -o $@

$(BUILD_DIR)/rz/sound.s: $(MOD_SOUND)
	$(RAZM) $(MOD_SOUND) -o $@

$(BUILD_DIR)/rz/play.s: $(MOD_PLAY)
	$(RAZM) $(MOD_PLAY) -o $@

$(BUILD_DIR)/rz/engine.s: $(MOD_ENGINE)
	$(RAZM) $(MOD_ENGINE) -o $@

$(BUILD_DIR)/rz/%.o: $(BUILD_DIR)/rz/%.s
	$(NASM) $< -o $@

$(BUILD_DIR)/c/%.o: ./%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

# temporary, to help debugging
# .PRECIOUS: $(BUILD_DIR)/rz/m_random.s

#----- Targets -----------------------------------------------

clean:
	rm -f $(PROGRAM) $(BUILD_DIR)/*/*.*
	rm -f core core.* ERRS

rzclean:
	rm -f $(BUILD_DIR)/rz/*.*

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

# this is used to create the BUILD_DIR directory
$(DUMMY):
	mkdir -p $(BUILD_DIR)/c
	mkdir -p $(BUILD_DIR)/rz
	@touch $@

.PHONY: all clean rzclean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
