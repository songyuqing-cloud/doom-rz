;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

;
; Keyboard controls
;
var key_right s32 = KEY_RIGHTARROW
var key_left  s32 = KEY_LEFTARROW
var key_up    s32 = KEY_UPARROW
var key_down  s32 = KEY_DOWNARROW

var key_strafeleft  s32 = ','
var key_straferight s32 = '.'

var key_fire   s32 = KEY_CTRL
var key_use    s32 = ' '
var key_strafe s32 = KEY_ALT
var key_speed  s32 = KEY_SHIFT
var key_jump   s32 = '/'

;
; Mouse buttons
;
var mouseb_fire         s32 = 0
var mouseb_strafe       s32 = 1
var mouseb_forward      s32 = 2
var mouseb_backward     s32 = -1
var mouseb_jump         s32 = -1

var mouseb_strafeleft   s32 = -1
var mouseb_straferight  s32 = -1
var mouseb_use          s32 = -1

var mouseb_prevweapon   s32 = -1
var mouseb_nextweapon   s32 = -1

; miscellaneous keys:

var key_msg_refresh s32 = KEY_ENTER
var key_pause       s32 = KEY_PAUSE
var key_demo_quit   s32 = 'q'
var key_spy         s32 = KEY_F12

; weapon selection keys:

var key_weapon1    s32 = '1'
var key_weapon2    s32 = '2'
var key_weapon3    s32 = '3'
var key_weapon4    s32 = '4'
var key_weapon5    s32 = '5'
var key_weapon6    s32 = '6'
var key_weapon7    s32 = '7'
var key_weapon8    s32 = '8'
var key_prevweapon s32 = 0
var key_nextweapon s32 = 0

; automap keys:

var key_map_north    s32 = KEY_UPARROW
var key_map_south    s32 = KEY_DOWNARROW
var key_map_east     s32 = KEY_RIGHTARROW
var key_map_west     s32 = KEY_LEFTARROW
var key_map_zoomin   s32 = '='
var key_map_zoomout  s32 = '-'
var key_map_toggle   s32 = KEY_TAB
var key_map_maxzoom  s32 = '0'
var key_map_follow   s32 = 'f'
var key_map_grid     s32 = 'g'
var key_map_mark     s32 = 'm'
var key_map_clearmark  s32 = 'c'

; menu keys:

var key_menu_activate s32 = KEY_ESCAPE
var key_menu_up       s32 = KEY_UPARROW
var key_menu_down     s32 = KEY_DOWNARROW
var key_menu_left     s32 = KEY_LEFTARROW
var key_menu_right    s32 = KEY_RIGHTARROW
var key_menu_back     s32 = KEY_BACKSPACE
var key_menu_forward  s32 = KEY_ENTER
var key_menu_confirm  s32 = 'y'
var key_menu_abort    s32 = 'n'

var key_menu_help     s32 = KEY_F1
var key_menu_save     s32 = KEY_F2
var key_menu_load     s32 = KEY_F3
var key_menu_volume   s32 = KEY_F4
var key_menu_detail   s32 = KEY_F5
var key_menu_qsave    s32 = KEY_F6
var key_menu_endgame  s32 = KEY_F7
var key_menu_messages s32 = KEY_F8
var key_menu_qload    s32 = KEY_F9
var key_menu_quit     s32 = KEY_F10
var key_menu_gamma    s32 = KEY_F11

var key_menu_incscreen  s32 = KEY_EQUALS
var key_menu_decscreen  s32 = KEY_MINUS
var key_menu_screenshot s32 = 0

; Control whether if a mouse button is double clicked, it acts like
; "use" has been pressed

var dclick_use s32 = 1

;
; Bind all of the common controls used by Doom and all other games.
;
fun M_BindBaseControls ()
	M_BindIntVariable "key_right"            key_right
	M_BindIntVariable "key_left"             key_left
	M_BindIntVariable "key_up"               key_up
	M_BindIntVariable "key_down"             key_down
	M_BindIntVariable "key_strafeleft"       key_strafeleft
	M_BindIntVariable "key_straferight"      key_straferight
	M_BindIntVariable "key_fire"             key_fire
	M_BindIntVariable "key_use"              key_use
	M_BindIntVariable "key_strafe"           key_strafe
	M_BindIntVariable "key_speed"            key_speed

	M_BindIntVariable "mouseb_fire"          mouseb_fire
	M_BindIntVariable "mouseb_strafe"        mouseb_strafe
	M_BindIntVariable "mouseb_forward"       mouseb_forward

	; Extra controls that are not in the Vanilla versions:
	M_BindIntVariable "mouseb_strafeleft"    mouseb_strafeleft
	M_BindIntVariable "mouseb_straferight"   mouseb_straferight
	M_BindIntVariable "mouseb_use"           mouseb_use
	M_BindIntVariable "mouseb_backward"      mouseb_backward
	M_BindIntVariable "key_pause"            key_pause
	M_BindIntVariable "key_message_refresh"  key_msg_refresh

	; Config items
	M_BindIntVariable "dclick_use"           dclick_use
end

fun M_BindWeaponControls ()
	M_BindIntVariable "key_weapon1"          key_weapon1
	M_BindIntVariable "key_weapon2"          key_weapon2
	M_BindIntVariable "key_weapon3"          key_weapon3
	M_BindIntVariable "key_weapon4"          key_weapon4
	M_BindIntVariable "key_weapon5"          key_weapon5
	M_BindIntVariable "key_weapon6"          key_weapon6
	M_BindIntVariable "key_weapon7"          key_weapon7
	M_BindIntVariable "key_weapon8"          key_weapon8

	M_BindIntVariable "key_prevweapon"       key_prevweapon
	M_BindIntVariable "key_nextweapon"       key_nextweapon

	M_BindIntVariable "mouseb_prevweapon"    mouseb_prevweapon
	M_BindIntVariable "mouseb_nextweapon"    mouseb_nextweapon
end

fun M_BindMapControls ()
	M_BindIntVariable "key_map_north"        key_map_north
	M_BindIntVariable "key_map_south"        key_map_south
	M_BindIntVariable "key_map_east"         key_map_east
	M_BindIntVariable "key_map_west"         key_map_west
	M_BindIntVariable "key_map_zoomin"       key_map_zoomin
	M_BindIntVariable "key_map_zoomout"      key_map_zoomout
	M_BindIntVariable "key_map_toggle"       key_map_toggle
	M_BindIntVariable "key_map_maxzoom"      key_map_maxzoom
	M_BindIntVariable "key_map_follow"       key_map_follow
	M_BindIntVariable "key_map_grid"         key_map_grid
	M_BindIntVariable "key_map_mark"         key_map_mark
	M_BindIntVariable "key_map_clearmark"    key_map_clearmark
end

fun M_BindMenuControls ()
	M_BindIntVariable "key_menu_activate"    key_menu_activate
	M_BindIntVariable "key_menu_up"          key_menu_up
	M_BindIntVariable "key_menu_down"        key_menu_down
	M_BindIntVariable "key_menu_left"        key_menu_left
	M_BindIntVariable "key_menu_right"       key_menu_right
	M_BindIntVariable "key_menu_back"        key_menu_back
	M_BindIntVariable "key_menu_forward"     key_menu_forward
	M_BindIntVariable "key_menu_confirm"     key_menu_confirm
	M_BindIntVariable "key_menu_abort"       key_menu_abort

	M_BindIntVariable "key_menu_help"        key_menu_help
	M_BindIntVariable "key_menu_save"        key_menu_save
	M_BindIntVariable "key_menu_load"        key_menu_load
	M_BindIntVariable "key_menu_volume"      key_menu_volume
	M_BindIntVariable "key_menu_detail"      key_menu_detail
	M_BindIntVariable "key_menu_qsave"       key_menu_qsave
	M_BindIntVariable "key_menu_endgame"     key_menu_endgame
	M_BindIntVariable "key_menu_messages"    key_menu_messages
	M_BindIntVariable "key_menu_qload"       key_menu_qload
	M_BindIntVariable "key_menu_quit"        key_menu_quit
	M_BindIntVariable "key_menu_gamma"       key_menu_gamma

	M_BindIntVariable "key_menu_incscreen"   key_menu_incscreen
	M_BindIntVariable "key_menu_decscreen"   key_menu_decscreen
	M_BindIntVariable "key_menu_screenshot"  key_menu_screenshot
	M_BindIntVariable "key_demo_quit"        key_demo_quit
	M_BindIntVariable "key_spy"              key_spy
end
